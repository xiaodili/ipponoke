# Ipponoke

Prior to running `/server npm run watch` (or `node dist/server/index.js` on production), make sure each of `ipponkese`, `server` and `webapp` bundles are built.

## Ipponokese

    # Run notation parsing tests
    npm run test
    
    # Run parser on /data directory songs
    npm run verify
    
    # Run parser on one song
    npm run verify v2/samples/guns-n-roses.song
    
    # Builds parser
    npm run build

## Webapp

    # Builds client for production
    npm run build:prod
    
    # Runs watcher
    npm run start
    
    # Visual tests
    npm run storybook

## Server

    # Builds client for production
    npm run build
    
    # Install runtime dependencies (Run this after installing new NPM packages)
    npm run install-deps

    # Runs watcher
    npm run watch
    
## Favicon

    # png converted from svg with Inkscape
    convert assets/favicon.png  -define icon:auto-resize dist/webapp/favicon.ico
    
## Infrastructure

    # build production version
    scripts/build.sh
    
    # deploy to EC2
    scripts/deploy.sh
    
    # If first time:

    # On EC2 machine
    pushd /apps/ipponoke
    scripts/install.sh

    sudo cp scripts/ipponoke.conf /etc/init

    
    
    

## Song format

### V1

YAML-esque metadata at the top, with square-bracketted tags to denote sections. Use dots to denote a chomp of the progression.

```

> Save Tonight

artist: Eagle Eye Cherry
key: a
themes: [love, loss]
link: "https://www.youtube.com/watch?v=zHxnm1-gVS4"

>> Intro, Verse, Chorus, Outro: i VI III VII

---

[Intro]

....

[Verse]
          .    .    .         .
Go on and close the curtains
              .   .         .     .
'Cause all we need is candlelight
        .  .      .         .
You and me, and a bottle of wine
   .          .      .    .
To hold you tonight (oh)

[Verse]
        .   .    .          .
Well we know I'm going away
          .    .   .                  .
And how I wish - I wish it weren't so
             .   .    .          .
So take this wine and drink with me
    .               .       .  .
And let's delay our misery     Save...

```

See `/ipponokese/v1/test_happy.js` for expected output.

### V2

YAML-esque metadata at the top, with square-bracketted tags to denote sections. Metadata can be defined for each section.

Progressions form is removed. This can be parsed later.

```

> Nemo

artist: Nightwish
key: d
themes: [self]
link: "https://www.youtube.com/watch?v=AGWBmZnZdkw"

[Intro]

Dm Dm C Bb

[Verse]

Dm         C
This is me for forever
Bb
One of the lost ones
    Dm            C
The one without a name
            Gm             Bb C
Without an honest heart as compass

[Verse]

key: f
youtube-time: "3:03"

Fm                   Eb
Walk the dark path, swim with angels
Db
Call the past for help
Bbm                 Fm
Touch me with your love
       Bbm          Db    Eb
And reveal to me my true name
```

See `/ipponokese/v2/test_happy.js` for expected output.

If specifying any value with e.g. colons, e.g. in `link` or `youtube-time`, make sure to enclose in string.
