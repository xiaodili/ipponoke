const majorDegrees = [
  'I',
  'bII',
  'II',
  'bIII',
  'III',
  'IV',
  'bV',
  'V',
  'bVI',
  'VI',
  'bVII',
  'VII',
];

const minorDegrees = [
  'i',
  'bii',
  'ii',
  'iii',
  'biv',
  'iv',
  'bv',
  'v',
  'vi',
  'bvii',
  'vii',
  'bi',
];

const majorMinorEnharmonics = [
  ['C', 'Am'],
  ['Db', 'Bbm'],
  ['D', 'Bm'],
  ['Eb', 'Cm'],
  ['E', 'C#m'],
  ['F', 'Dm'],
  ['Gb', 'Ebm'],
  ['G', 'Em'],
  ['Ab', 'Fm'],
  ['A', 'F#m'],
  ['Bb', 'Gm'],
  ['B', 'G#m'],
];

const degreesLookup = new Set(majorDegrees.concat(minorDegrees));

// https://www.pianoscales.org/major.html
// https://www.pianoscales.org/minor.html
// Convention: For enharmonic scales, Use flats for major keys, use sharps for minor keys

const keys = [
  ['C'],
  ['Db', 'C#'],
  ['D'],
  ['Eb', 'D#'],
  ['E'],
  ['F'],
  ['Gb', 'F#'],
  ['G'],
  ['Ab', 'G#'],
  ['A'],
  ['Bb', 'A#'],
  ['B'],
];

const majorKeys = keys.map(key => key[0]);
const minorKeys = keys.map(key => key[key.length - 1]);

// 'C': 0,
// 'Db': 1,
// 'C#': 1,
// 'D': 2,
// 'Eb': 3,
// 'D#': 3
// etc.
const keyIntervalLookup = Object.assign({}, ...keys.map((enharmonics, idx) => {
  return Object.assign({}, ...enharmonics.map((key, _) => {
    return {
      [key]: idx
    };
  }))
}));

const colours = [
  'add9', //add 9 (same as an add2?)
  'add2', //add 2
  'add4', 
  'aug', // augmented (only for major chords)
  'sus2', //suspended chord 2
  'sus4', //suspended chord 4
  'sus7', //suspended chord
  'sus', //suspended chord (implied 4th)
  'maj7', //major seventh (major chord with major seventh)
  'm', //minor (only for letter chords)
  'dim', //diminished chord (root, two minor thirds)
  '5', //power chord
  '6', //major sixth
  '7', //dominant seventh (major chord + minor seventh)
  '9', //dominant ninth
  'b9', //flat ninth
  // '/V', //Secondary dominant (by function) https://music.stackexchange.com/questions/22057/what-is-a-secondary-dominant-chord (normally II by form)
  // '/', // Slash chord
];


const constructChordRegex = () => {
  // rootsFlattened: [ 'C', 'Db', 'C#', 'D', 'Eb', 'D#', 'E', 'F', 'Gb', 'F#', 'G', 'Ab', 'G#', 'A', 'Bb', 'A#', 'B', 'I', 'bII', 'II', 'bIII', 'III', 'IV', 'bV', 'V', 'bVI', 'VI', 'bVII', 'VII', 'i', 'bii', 'ii', 'iii', 'biv', 'iv', 'bv', 'v', 'vi', 'bvii', 'vii', 'bi' ]
  // const rootsFlattened = keys.reduce((acc, val) => acc.concat(val), []).concat(majorDegrees).concat(minorDegrees);
  const rootsFlattenedSorted = keys.reduce((acc, val) => acc.concat(val), []).concat(majorDegrees).concat(minorDegrees).sort(function(a, b) {
    return b.length - a.length;
  });

  // 3 (+ 1 slash) colours is hopefully
  const chordRegexString = `^(${rootsFlattenedSorted.join('|')})\
(${colours.join('|')})?\
(${colours.join('|')})?\
(${colours.join('|')})?\
(/)?\
(${rootsFlattenedSorted.join('|')})?$`

  return new RegExp(chordRegexString);
}

const chordRegex = constructChordRegex();

const romanIsMajor = (roman) => {
  return !(roman.toLowerCase() === roman);
}

const letterColoursIsMajor = (colours) => {
  return !(colours[0] === 'm');
}

const parseChord = (chordStr) => {

  var matches = chordRegex.exec(chordStr);

  if (!matches) {
    throw new Error(`Chord ${chordStr} not recognised.`)
  }

  const root = matches[1];
  let colours = matches.slice(2).filter(m => m);


  const isRoman = degreesLookup.has(root);

  let isMajor;

  // getting chord type
  if (isRoman) {
    isMajor = romanIsMajor(root);
  } else { // letter
    isMajor = letterColoursIsMajor(colours);

    if (!isMajor) {
      colours = colours.slice(1);
    }
  }

  const chord = {
    type: isRoman ? 'roman' : 'letter',
    root: isRoman ? root.toLowerCase() : root,
    isMajor,
  };

  let slashIndex = colours.indexOf('/');
  if (slashIndex !== -1) {
    let slashNote = colours[slashIndex + 1];
    colours = colours.slice(0, slashIndex)
    chord.slashNote = slashNote;
  }

  chord.colours = colours;

  return chord;
}

// Only doing the common ones
const majorIntervalToNumeralLookup = {
  0: 'i',
  2: 'ii',
  3: 'biii',
  4: 'iii',
  5: 'iv',
  7: 'v',
  9: 'vi',
  10: 'bvii',
  11: 'vii'
};

const minorIntervalToNumeralLookup = {
  0: 'i',
  1: 'bii',
  2: 'ii',
  3: 'iii',
  5: 'iv',
  6: 'bv',
  7: 'v',
  8: 'vi',
  10: 'vii',
};

// Inverts the above lookups (need to convert to int)
const invert = (intervalToNumeral) => {

  const inverted = {};

  for (const k in intervalToNumeral) {
    inverted[intervalToNumeral[k]] = parseInt(k);
  }

  return inverted;
}

const majorNumeralToIntervalLookup = invert(majorIntervalToNumeralLookup);
const minorNumeralToIntervalLookup = invert(minorIntervalToNumeralLookup);

const intervalToNumeral = (isMajor, interval) => {
  if (isMajor) {
    if (interval in majorIntervalToNumeralLookup) {
      return majorIntervalToNumeralLookup[interval];
    } else {
      throw new Error(`Interval ${interval} is not present in major scale`);
    }
  } else {
    if (interval in minorIntervalToNumeralLookup) {
      return minorIntervalToNumeralLookup[interval];
    } else {
      throw new Error(`Interval ${interval} is not present in minor scale`);
    }
  }
}

const numeralToInterval = (isMajor, numeral) => {

  if (isMajor) {
    if (numeral in majorNumeralToIntervalLookup) {
      return majorNumeralToIntervalLookup[numeral];
    } else {
      throw new Error(`Numeral ${numeral} is not present in major scale`);
    }
  } else {
    if (numeral in minorNumeralToIntervalLookup) {
      return minorNumeralToIntervalLookup[numeral];
    } else {
      throw new Error(`Numeral ${numeral} is not present in minor scale`);
    }
  }
}

const minorToMajorEnharmonic = {
  i: 'vi',
  I: 'VI',
  bII: 'bVII',
  ii: 'vii',
  iii: 'i',
  III: 'I',
  iv: 'ii',
  IV: 'II',
  v: 'iii',
  V: 'III',
  VI: 'IV',
  VII: 'V',
}

const getLetterInterval = (root, letter) => {

  const rootPos = keyIntervalLookup[root];
  const letterPos = keyIntervalLookup[letter];

  const interval = letterPos - rootPos;

  return interval < 0 ? keys.length + interval : interval;
}

const parseScale = (scale) => {

  if (scale in keyIntervalLookup) {

    return {
      root: scale,
      isMajor: true,
    };

  } else {

    return {
      root: scale.slice(0, -1),
      isMajor: false
    };
  }
}

const chordLetterToRoman = (chord, scale) => {

  if (chord.type === 'roman') {
    throw new TypeError(`Chord ${chord.root} already in roman`);
  }

  const {
    root,
    isMajor,
  } = parseScale(scale);

  const interval = getLetterInterval(root, chord.root);

  if (Number.isNaN(interval)) {
    throw new TypeError(`Cannot find interval of ${chord.root} in ${scale}`);
  }

  let numeral;
  try {
    numeral = intervalToNumeral(isMajor, interval);
  } catch(error) {
    throw new TypeError(`chord ${JSON.stringify(chord)} not found in ${scale}`)
  }

  const romanChord = {
    type: 'roman',
    root: numeral,
    isMajor: chord.isMajor,
    colours: chord.colours
  };

  if (chord.slashNote) {
    const slashInterval = getLetterInterval(root, chord.slashNote);
    const slashNumeral = intervalToNumeral(isMajor, slashInterval);
    romanChord.slashNote = slashNumeral;
  }
  return romanChord;
}

const majorUseFlats = new Set([
  'Db',
  'Eb',
  'F',
  'Gb',
  'Ab',
  'Bb',
]);

const minorUseFlats = new Set([
  'Bb',
  'C',
  'Db',
  'D',
  'Eb',
  'F',
  'G',
  'Ab',
]);

const chordRomanToLetter = (chord, scale) => {

  if (chord.type === 'letter') {
    throw new TypeError(`Chord ${chord.root} already in letter`);
  }

  const {
    root,
    isMajor,
  } = parseScale(scale);

  const rootKeyIdx = keyIntervalLookup[root];

  const interval = numeralToInterval(isMajor, chord.root);
  const letterIdx = (interval + rootKeyIdx) % keys.length;
  const letterRoot = (isMajor && majorUseFlats.has(root)) || (!isMajor && minorUseFlats.has(root))
        ? keys[letterIdx][0]
        : keys[letterIdx][keys[letterIdx].length - 1]

  const letterChord = {
    type: 'letter',
    root: letterRoot,
    isMajor: chord.isMajor,
    colours: chord.colours,
  };

  if (chord.slashNote) {
    const slashInterval = numeralToInterval(isMajor, chord.slashNote);
    const slashIdx = (slashInterval + keyIntervalLookup[root]) % keys.length;
    const slashKey = (isMajor && majorUseFlats.has(root)) || (!isMajor && minorUseFlats.has(root))
          ? keys[slashIdx][0]
          : keys[slashIdx][keys[slashIdx].length - 1];
    letterChord.slashNote = slashKey;
  }

  return letterChord;
}

const lineContainsOnlyChords = (line) => {
  const maybeChords = line.trim().split(/\s+/);
  for (const chordStr of maybeChords) {
    if (!chordRegex.exec(chordStr)) {
      return false;
    }
  }
  return true;
}

const transposeScale = (originalKey, currentScale, sectionKey) => {

  if (currentScale === 'roman') {
    const keyShift = keyIntervalLookup[parseScale(sectionKey).root] - keyIntervalLookup[parseScale(originalKey).root];
    return `${keyShift > 0 ? '+' : '-'}${keyShift}`;
  }

  const keyDiff = keyIntervalLookup[parseScale(currentScale).root] - keyIntervalLookup[parseScale(originalKey).root];

  const {
    isMajor,
    root
  } = parseScale(sectionKey);

  let newSectionInterval = keyIntervalLookup[root] + keyDiff;

  if (newSectionInterval < 0) {
    newSectionInterval += keys.length;
  } else if (newSectionInterval >= keys.length) {
    newSectionInterval -= keys.length;
  }

  if (isMajor) {
    return keys[newSectionInterval][0];
  } else {
    return keys[newSectionInterval][keys[newSectionInterval].length - 1] + 'm';
  }
}

const validMajorScales = [
  'C',
  'C#',
  'Db',
  'D',
  'Eb',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'Ab',
  'A',
  'Bb',
  'B',
];

const validMinorScales = [
  'A',
  'A#',
  'Bb',
  'B',
  'C',
  'C#',
  'Db',
  'D',
  'D#',
  'Eb',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'Ab',
];

const renderLetterChord = (chord) => {

  let renderedRoot;

  if (chord.isMajor) {
    renderedRoot = chord.root;
  } else {
    renderedRoot = `${chord.root}m`;
  }

  let renderedSlashNote = '';

  if (chord.slashNote) {
    renderedSlashNote = `/${chord.slashNote}`;
  }

  return `${renderedRoot}${chord.colours.join('')}${renderedSlashNote}`;
};

const renderRomanChord = (chord) => {
  
  let renderedRoot;

  if (chord.isMajor) {
    if (chord.root[0] === 'b') {
      renderedRoot = `b${chord.root.slice(1).toUpperCase()}`;
    } else {
      renderedRoot = chord.root.toUpperCase();
    }
  } else {
    renderedRoot = chord.root;
  }

  let renderedSlashNote = '';

  if (chord.slashNote) {
    renderedSlashNote = `/${chord.slashNote.toUpperCase()}`;
  }

  return `${renderedRoot}${chord.colours.join('')}${renderedSlashNote}`;
};

exports.majorKeys = majorKeys;
exports.minorKeys = minorKeys;
exports.parseChord = parseChord;
exports.parseScale = parseScale;
exports.chordLetterToRoman = chordLetterToRoman;
exports.chordRomanToLetter = chordRomanToLetter;
exports.lineContainsOnlyChords = lineContainsOnlyChords;
exports.transposeScale = transposeScale;
exports.validMajorScales = validMajorScales;
exports.validMinorScales = validMinorScales;
exports.renderLetterChord = renderLetterChord;
exports.renderRomanChord = renderRomanChord;
exports.majorMinorEnharmonics = majorMinorEnharmonics;
