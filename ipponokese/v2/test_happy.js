const fs = require('fs');
const path = require('path');

const peg = require('pegjs');
const _ = require('lodash');

const GRAMMAR_PATH = 'grammar.pegjs';

const grammar = fs.readFileSync(path.join(__dirname, GRAMMAR_PATH), 'utf8');
const parser = peg.generate(grammar);

const test_song = `
> Nemo

artist: Florence! & the Machine
key: d
themes: [self]
link: "https://www.youtube.com/watch?v=AGWBmZnZdkw"

[Intro]

Dm Dm C Bb

[Verse 1]

 Dm         C
This is me for forever
Bb
One of the lost ones
    Dm            C
The one without a name
            Gm             Bb C
Without an honest heart as compass

[Pre-Chorus]

key: f

Fm                   Eb
Walk the dark path, swim with angels
Db
Call the past for help
Bbm                 Fm
Touch me with your love
       Bbm          Db    Eb
And reveal to me my true name

`

const actual = parser.parse(test_song);
const expected = {
  "songs": [
    {
      "title": "Nemo",
      "metadata": {
        "artist": "Florence! & the Machine",
        "key": "d",
        "themes": [
          "self"
        ],
        "link": "https://www.youtube.com/watch?v=AGWBmZnZdkw"
      },
      "sections": [
        {
          "name": "Intro",
          "metadata": {},
          "lines": [
            "Dm Dm C Bb"
          ]
        },
        {
          "name": "Verse 1",
          "metadata": {},
          "lines": [
            " Dm         C",
            "This is me for forever",
            "Bb",
            "One of the lost ones",
            "    Dm            C",
            "The one without a name",
            "            Gm             Bb C",
            "Without an honest heart as compass"
          ]
        },
        {
          "name": "Pre-Chorus",
          "metadata": {
            "key": "f"
          },
          "lines": [
            "Fm                   Eb",
            "Walk the dark path, swim with angels",
            "Db",
            "Call the past for help",
            "Bbm                 Fm",
            "Touch me with your love",
            "       Bbm          Db    Eb",
            "And reveal to me my true name"
          ]
        }
      ]
    }
  ]
}

console.assert(_.isEqual(actual, expected));

