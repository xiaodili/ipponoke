// Ipponoke Grammar V2
// ===================

start
  = _  songs:songList? _ {
    return {
      songs: songs ? songs : []
    };
  }

songList
  = first:song rest:(_ inner:song { return inner; } )*
  { return [first].concat(rest); }

song
  = header:songHeader _ meta:metadata? _  sections: (_ ss:sectionList? { return ss; })?
  { return {
      title: header,
      metadata: meta ? meta : {},
      sections: sections ? sections : []
    };
  }

// Header
// ------
songHeader
  = '>' wsnb title:(c1:[^>] rest:anythingInlineString {return c1 + rest; })
  { return title; }

sectionList
  = values:(
      head:section
      tail:(newline_separator s:section { return s; })*
      { return [head].concat(tail); }
    )
    { return values; }

anythingInlineString
  = chars:anythingInline* { return chars.join(""); }

anythingInline
  = [^\n]

// Metadata
// --------
metadata
  = values:(
      head:keyValue
      tail:(meta_separator v:keyValue { return v; })*
      {
        var metadata = {}, i
          if (head) {
            metadata[head[0]] = head[1]
          }
          if (tail) {
            for (i = 0; i < tail.length; i++) {
              metadata[tail[i][0]] = tail[i][1]
            }
          }
        return metadata;
      }
    )
  { return values; }

keyValue
  = key:string key_value_separator value:allowedMetaValue wsnb
  {return [key, value]; }

allowedMetaValue
  = array
  / string

array
  = begin_array
    values:(
      head:string
      tail:(element_separator v:string { return v; })*
      { return [head].concat(tail); }
    )?
    end_array
    { return values !== null ? values : []; }

section
  = header:sectionHeader meta:(_ metadata)? wsnb nl lines:lineList?
    { return {
        name: header,
        metadata: meta ? meta[1] : {},
        lines: lines ? lines : []
      }
    }

lineList
  = values:(
      head:allowedLineString
      tail:(newline_separator t:allowedLineString { return t; })*
      { return [head].concat(tail); }
    )
    { return values; }

sectionHeader
  = '[' wsnb name:sectionString wsnb ']'
    {return name; }

sectionString
  = chars:allowedSectionChar* { return chars.join(""); }

allowedLineString
  = c1:[^\[>] chars:anythingInline* { return c1 + chars.join(""); }

// Lower Level Stuff
// -----------------

string
  = single_quotation_mark chars:singleQuotedChar* single_quotation_mark { return chars.join(""); }
  / double_quotation_mark chars:doubleQuotedChar* double_quotation_mark { return chars.join(""); }
  / unquotedString
_
  = ws
  / comment

nl
  = [\n]+

// lb
//  = [ \t\r\n]*[\n

ws
  = [ \t\r\n]*

comment
  = "//" ([^\n])*

wsnb
  = [ \t]*

allowedSectionChar
  = [A-Za-z0-9 /.?=\-_#&]

doubleQuotedChar
  = quotedChar
  / escape
    sequence: (
      '"'
    )
    { return sequence; }

singleQuotedChar
  = quotedChar
  / escape
    sequence: (
      "'"
    )
    { return sequence; }

unquotedString
  = chars:unquotedChar* { return chars.join(""); }

quotedChar
  = [ :A-Za-z0-9!/.?=\-_#&]

unquotedChar
  = [ A-Za-z0-9!/.?=\-_#&]

element_separator = wsnb "," wsnb
escape         = "\\"
double_quotation_mark = '"'
single_quotation_mark = "'"
meta_separator = newline_separator
newline_separator = [\n]+
key_value_separator = wsnb ":" wsnb
begin_array     = wsnb "[" wsnb
end_array       = wsnb "]" wsnb
