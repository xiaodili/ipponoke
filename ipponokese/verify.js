const path = require('path');
const fs = require('fs');
const parser = require('../dist/ipponokese/grammar.js');

const relDataPath = '../../ipponokese-data/songs'

const verifyFile = (absFilePath) => {

    console.log(`verifying ${absFilePath}...`);

    const songContents = fs.readFileSync(absFilePath, 'utf8');
    const songs = parser.parse(songContents).songs;

    for (const song of songs) {
      if (!song.metadata.key) {
        console.log(`No key present for song ${song.title}`);
      }
    }
}

const verifyFiles = (absDataPath) => {

  fs.readdirSync(absDataPath).forEach((file) => {

    const absFilePath = path.resolve(absDataPath, file);
    verifyFile(absFilePath);
  })
}

if (require.main === module) {

  let files = process.argv.slice(2);

  if (files.length === 0 ) {

    verifyFiles(relDataPath);
  } else {

    files.forEach((filePath) => {
      verifyFile(filePath);
    })
  }

  console.log(`done!`);
}
