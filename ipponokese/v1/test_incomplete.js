const fs = require('fs')
const path = require('path')
const GRAMMAR_PATH = 'grammar.pegjs'
const peg = require('pegjs')
const grammar = fs.readFileSync(path.join(__dirname, GRAMMAR_PATH), 'utf8')
const parser = peg.generate(grammar)

const it = (description, fn) => {
  fn()
}

const test_song = `
> Save Tonight

artist: Eagle Eye Cherry
`

const result = parser.parse(test_song)

console.log("result: ", JSON.stringify(result, null, 2));

it('should parse the basic header', () => {
  const test_string = `>Save Tonight`
  const result = parser.parse(test_string)
  console.log("result: ", result);
})

it('should allow newlines around there', () => {
  const test_string = `> Save Tonight`
  const result = parser.parse(test_string)
  console.log("result: ", result);
})

it("shouldn't allow newlines in title/metadata needs a key", () => {
  const test_string = `> Save
Tonight`
  const result = parser.parse(test_string)
  console.log("result: ", result);
})
