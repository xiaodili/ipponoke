
const fs = require('fs');
const path = require('path');

const peg = require('pegjs');
const _ = require('lodash');

const GRAMMAR_PATH = 'grammar.pegjs';

const grammar = fs.readFileSync(path.join(__dirname, GRAMMAR_PATH), 'utf8');
const parser = peg.generate(grammar);

const test_song = `
> Save Tonight

artist: Eagle Eye Cherry
key: a
themes: [love, loss]
link: "https://www.youtube.com/watch?v=zHxnm1-gVS4"

>> Intro, Verse, Chorus, Outro: i VI III VII

---

[Intro]

....

[Verse]
          .    .    .         .
Go on and close the curtains
              .   .         .     .
'Cause all we need is candlelight
        .  .      .         .
You and me, and a bottle of wine
   .          .      .    .
To hold you tonight (oh)

[Verse]
        .   .    .          .
Well we know I'm going away
          .    .   .                  .
And how I wish - I wish it weren't so
             .   .    .          .
So take this wine and drink with me
    .               .       .  .
And let's delay our misery     Save...

[Chorus]
     .    .    .                  .
...tonight and fight the break of dawn
       .     .    .              .
Come tomorrow - tomorrow I'll be gone
       .    .    .                  .
Save tonight and fight the break of dawn
       .     .    .              .
Come tomorrow - tomorrow I'll be gone

[Verse]
          .  .       .    .
There's a log on the fire
       .    .     .          .
And it burns like me for you
         .    .           .      .
Tomorrow comes with one desire
   .        .    .        .
To take me away (ohh it's true)
`
const expected = {
  "songs": [
    {
      "title": "Save Tonight",
      "metadata": {
        "artist": "Eagle Eye Cherry",
        "key": "a",
        "themes": [
          "love",
          "loss"
        ],
        "link": "https://www.youtube.com/watch?v=zHxnm1-gVS4"
      },
      "progressions": [
        [
          [
            "Intro",
            "Verse",
            "Chorus",
            "Outro"
          ],
          [
            [
              "i",
              "VI",
              "III",
              "VII"
            ]
          ]
        ]
      ],
      "sections": [
        {
          "section": "Intro",
          "lines": [
            "...."
          ]
        },
        {
          "section": "Verse",
          "lines": [
            "          .    .    .         .",
            "Go on and close the curtains",
            "              .   .         .     .",
            "'Cause all we need is candlelight",
            "        .  .      .         .",
            "You and me, and a bottle of wine",
            "   .          .      .    .",
            "To hold you tonight (oh)"
          ]
        },
        {
          "section": "Verse",
          "lines": [
            "        .   .    .          .",
            "Well we know I'm going away",
            "          .    .   .                  .",
            "And how I wish - I wish it weren't so",
            "             .   .    .          .",
            "So take this wine and drink with me",
            "    .               .       .  .",
            "And let's delay our misery     Save..."
          ]
        },
        {
          "section": "Chorus",
          "lines": [
            "     .    .    .                  .",
            "...tonight and fight the break of dawn",
            "       .     .    .              .",
            "Come tomorrow - tomorrow I'll be gone",
            "       .    .    .                  .",
            "Save tonight and fight the break of dawn",
            "       .     .    .              .",
            "Come tomorrow - tomorrow I'll be gone"
          ]
        },
        {
          "section": "Verse",
          "lines": [
            "          .  .       .    .",
            "There's a log on the fire",
            "       .    .     .          .",
            "And it burns like me for you",
            "         .    .           .      .",
            "Tomorrow comes with one desire",
            "   .        .    .        .",
            "To take me away (ohh it's true)"
          ]
        }
      ]
    }
  ]
}

const actual = parser.parse(test_song);

console.assert(_.isEqual(actual, expected));
