// Ipponoke Grammar V1
// ===================

start
  = _  songs:songList? _ {
    return {
      songs: songs ? songs : []
    };
  }

songList
  = first:song rest:(_ inner:song { return inner; } )*
  { return [first].concat(rest); }

song
  = header:songHeader _ meta:metadata? _ progs:progressions? sections:(_ '---' _ ss:sectionList? { return ss; })?
  { return {
      title: header,
      metadata: meta ? meta : {},
      progressions: progs ? progs : [],
      sections: sections ? sections : []
    };
  }

sectionList
  = values:(
      head:section
      tail:(newline_separator s:section { return s; })*
      { return [head].concat(tail); }
    )
    { return values; }

section
  = header:sectionHeader wsnb lines:(newline_separator ls:lineList { return ls; })?
    { return {
        section: header,
        lines: lines ? lines : []
      }
    }

sectionHeader
  = '[' wsnb name:sectionString wsnb ']'
    {return name; }

lineList
  = values:(
      head:allowedLineString
      tail:(newline_separator t:allowedLineString { return t; })*
      { return [head].concat(tail); }
    )
    { return values; }

// Header
// ------
songHeader
  = '>' wsnb title:(c1:[^>] rest:anythingInlineString {return c1 + rest; })
  { return title; }

allowedLineString
  /*= chars:[^\[^\]^\n]* { return chars.join(""); }*/
  /*= c1:[^\[]+ chars:anythingInline* cn:[^\]] { return c1.join('') + chars.join("") + cn; }*/
  = c1:[^\[>] chars:anythingInline* { return c1 + chars.join(""); }
  /*= chars:allowedLineCharacters* { return chars.join(""); }*/

allowedLineCharacters
  = [^(\n\[\]>)]
  /*= [^\n^\[^\]^>]*/

anythingInlineString
  = chars:anythingInline* { return chars.join(""); }

anythingInline
  = [^\n]

// Metadata
// --------
metadata
  = values:(
      head:keyValue
      tail:(meta_separator v:keyValue { return v; })*
      {
        var metadata = {}, i
          if (head) {
            metadata[head[0]] = head[1]
          }
          if (tail) {
            for (i = 0; i < tail.length; i++) {
              metadata[tail[i][0]] = tail[i][1]
            }
          }
        return metadata;
      }
    )
  { return values; }

keyValue
  = key:string key_value_separator value:allowedMetaValue wsnb
  {return [key, value]; }

allowedMetaValue
  = array
  / string

array
  = begin_array
    values:(
      head:string
      tail:(element_separator v:string { return v; })*
      { return [head].concat(tail); }
    )?
    end_array
    { return values !== null ? values : []; }


// Progressions
// ------------

progressions
  = values:(
      head: progression
      tail: (newline_separator p:progression { return p; })*
      { return [head].concat(tail); }
    )
    { return values ? values : []; }
  

progression
  = '>>' wsnb sectionNames:sectionNameList key_value_separator annotation:annotationSpec
  { return [sectionNames, annotation]; }

sectionNameList
  = values:(
      head: sectionString
      tail: (element_separator s:sectionString { return s; })*
      { return [head].concat(tail); }
    )
    { return values; }

sectionString
  = chars:allowedSectionChar* { return chars.join(""); }

annotationSpec
  = chords:chordList key:(element_separator k:keySignature { return k; })?
    { return key ? [chords, key] : [chords]; }

keySignature
  = chars:allowedKeySigChar* { return chars.join(""); }

chordList
  = values:(
      head: chord
      tail: (chord_separator c:chord { return c; })*
      { return [head].concat(tail); }
    )
    { return values; }

chord
  = chars:allowedChordChar* { return chars.join(""); }

allowedKeySigChar
  = [A-Za-z]

allowedChordChar
  = [A-Za-z0-9\-]

allowedSectionChar
  = [A-Za-z0-9]

// Lower Level Stuff
// -----------------

string
  = single_quotation_mark chars:singleQuotedChar* single_quotation_mark { return chars.join(""); }
  / double_quotation_mark chars:doubleQuotedChar* double_quotation_mark { return chars.join(""); }
  / unquotedString

doubleQuotedChar
  = quotedChar
  / escape
    sequence: (
      '"'
    )
    { return sequence; }

singleQuotedChar
  = quotedChar
  / escape
    sequence: (
      "'"
    )
    { return sequence; }

unquotedString
  = chars:unquotedChar* { return chars.join(""); }

//TODO: make these more sophisticated
quotedChar
  = [ :A-Za-z0-9/.?=\-#]

unquotedChar
  = [ A-Za-z0-9/.?=\-#]
 
element_separator = wsnb "," wsnb
meta_separator = newline_separator
prog_separator = newline_separator
chord_separator = [ \t]+
newline_separator = [\n]+
key_value_separator = wsnb ":" wsnb
begin_array     = wsnb "[" wsnb
end_array       = wsnb "]" wsnb
_
  = ws
  / comment

comment
  = "//" ([^\n])*

wsnb
  = [ \t]*

ws
  = [ \t\r\n]*

escape         = "\\"
double_quotation_mark = '"'
single_quotation_mark = "'"
