const {
  expect
} = require('chai');

const {
  describe,
  it
} = require('mocha');

const {
  parseChord,
  chordLetterToRoman,
  chordRomanToLetter,
  lineContainsOnlyChords,
  renderRomanChord,
  renderLetterChord,
  transposeScale,
} = require('./notation.js');


describe('chord parsing', () => {

  it('should parse numeral chords', () => {

    expect(parseChord('IV')).to.deep.equal({
      type: 'roman',
      root: 'iv',
      colours: [],
      isMajor: true,
    });

    expect(parseChord('II7')).to.deep.equal({
      type: 'roman',
      root: 'ii',
      colours: ['7'],
      isMajor: true,
    });

    expect(parseChord('II7/I')).to.deep.equal({
      type: 'roman',
      root: 'ii',
      colours: ['7'],
      slashNote: 'I',
      isMajor: true,
    });

    expect(parseChord('i')).to.deep.equal({
      type: 'roman',
      root: 'i',
      isMajor: false,
      colours: [],
    });

    expect(parseChord('V7sus4')).to.deep.equal({
      type: 'roman',
      root: 'v',
      isMajor: true,
      colours: ['7', 'sus4'],
    });
  });

  it('should parse explicit chords', () => {

    expect(parseChord('Csus2')).to.deep.equal({
      type: 'letter',
      root: 'C',
      isMajor: true,
      colours: ['sus2']
    });

    expect(parseChord('Cm')).to.deep.equal({
      type: 'letter',
      root: 'C',
      isMajor: false,
      colours: []
    });

    expect(parseChord('F#m')).to.deep.equal({
      type: 'letter',
      root: 'F#',
      isMajor: false,
      colours: []
    });

    expect(parseChord('Eadd4')).to.deep.equal({
      type: 'letter',
      root: 'E',
      isMajor: true,
      colours: ['add4']
    });

    expect(parseChord('G#m/F#')).to.deep.equal({
      type: 'letter',
      root: 'G#',
      isMajor: false,
      colours: [],
      slashNote: 'F#'
    });

    expect(parseChord('Dmadd9')).to.deep.equal({
      type: 'letter',
      root: 'D',
      isMajor: false,
      colours: ['add9']
    });

    expect(parseChord('D7sus4')).to.deep.equal({
      type: 'letter',
      root: 'D',
      isMajor: true,
      colours: ['7', 'sus4']
    });

    expect(parseChord('Gbmaj7')).to.deep.equal({
      type: 'letter',
      root: 'Gb',
      isMajor: true,
      colours: ['maj7']
    });
    
    // // Don't do lower case letter chords
    expect(() => parseChord('c')).to.throw(Error, 'Chord c not recognised.');
  });
});

describe('chord converting', () => {

  it('should convert letter chords to roman chords', () => {

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'C',
      isMajor: true,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'roman',
      root: 'i',
      isMajor: true,
      colours: []
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'C',
      isMajor: false,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'roman',
      root: 'i',
      isMajor: false,
      colours: []
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'A',
      isMajor: false,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'roman',
      root: 'vi',
      isMajor: false,
      colours: []
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'Bb',
      isMajor: true,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'roman',
      root: 'bvii',
      isMajor: true,
      colours: []
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'C',
      isMajor: true,
      colours: []
    }, 'Am')).to.deep.equal({
      type: 'roman',
      root: 'iii',
      isMajor: true,
      colours: []
    });

    expect(() => chordLetterToRoman({
      type: 'letter',
      root: 'C#',
      colour: ''
    }, 'Am')).to.throw(Error, 'chord {"type":"letter","root":"C#","colour":""} not found in Am');

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'A',
      isMajor: true,
      colours: ['sus7']
    }, 'D')).to.deep.equal({
      type: 'roman',
      root: 'v',
      isMajor: true,
      colours: ['sus7']
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'F',
      isMajor: false,
      colours: []
    }, 'Ab')).to.deep.equal({
      type: 'roman',
      root: 'vi',
      isMajor: false,
      colours: []
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'A',
      isMajor: false,
      colours: ['7']
    }, 'G')).to.deep.equal({
      type: 'roman',
      root: 'ii',
      isMajor: false,
      colours: ['7']
    });

    expect(chordLetterToRoman({
      type: 'letter',
      root: 'G#',
      isMajor: false,
      slashNote: 'F#',
      colours: []
    }, 'B')).to.deep.equal({
      type: 'roman',
      root: 'vi',
      isMajor: false,
      slashNote: 'v',
      colours: []
    });

    // Flattened third
    expect(chordLetterToRoman({
      type: 'letter',
      root: 'Bb',
      isMajor: true,
      colours: []
    }, 'G')).to.deep.equal({
      type: 'roman',
      root: 'biii',
      isMajor: true,
      colours: []
    });

  });

  it('should convert roman chords to letter chords', () => {

    expect(chordRomanToLetter({
      type: 'roman',
      root: 'i',
      isMajor: true,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'letter',
      root: 'C',
      isMajor: true,
      colours: []
    });

    expect(chordRomanToLetter({
      type: 'roman',
      root: 'ii',
      isMajor: false,
      colours: []
    }, 'D')).to.deep.equal({
      type: 'letter',
      root: 'E',
      isMajor: false,
      colours: []
    });

    expect(chordRomanToLetter({
      type: 'roman',
      root: 'ii',
      isMajor: true,
      colours: []
    }, 'C')).to.deep.equal({
      type: 'letter',
      root: 'D',
      isMajor: true,
      colours: []
    });

    expect(chordRomanToLetter({
      type: 'roman',
      root: 'iii',
      isMajor: false,
      colours: [],
    }, 'D')).to.deep.equal({
      type: 'letter',
      root: 'F#',
      isMajor: false,
      colours: [],
    });

    expect(chordRomanToLetter({
      type: 'roman',
      root: 'vi',
      isMajor: false,
      slashNote: 'v',
      colours: [],
    }, 'B')).to.deep.equal({
      type: 'letter',
      root: 'G#',
      isMajor: false,
      slashNote: 'F#',
      colours: [],
    });
  });

  // it('should be able to enharmonise correctly', () => {

  //   expect(chordRomanToLetter({
  //     type: 'roman',
  //     root: 'i',
  //     colour: ''
  //   }, 'C', 1)).to.deep.equal({
  //     type: 'letter',
  //     root: 'A',
  //     colour: 'm'
  //   });

  //   expect(chordRomanToLetter({
  //     type: 'roman',
  //     root: 'III',
  //     colour: ''
  //   }, 'C', 1)).to.deep.equal({
  //     type: 'letter',
  //     root: 'C',
  //     colour: ''
  //   });

  //   expect(chordRomanToLetter({
  //     type: 'roman',
  //     root: 'I',
  //     colour: ''
  //   }, 'Am', -1)).to.deep.equal({
  //     type: 'letter',
  //     root: 'C',
  //     colour: ''
  //   });
  // })

});

describe('chord line classification', () => {

  it('should distinguish between lyrics and chord lines', () => {

    expect(lineContainsOnlyChords("I don't wanna miss a thing")).to.be.false;
    expect(lineContainsOnlyChords("A Big     Elephant")).to.be.false;
    expect(lineContainsOnlyChords("A Am      G F")).to.be.true;

    // Jazzy chords
    expect(lineContainsOnlyChords("Em6add9 C7b9")).to.be.true;
  })
});

describe('chord rendering', () => {

  it('should render roman chords', () => {

    expect(renderRomanChord({
      type: 'roman',
      root: 'vi',
      isMajor: false,
      slashNote: 'v',
      colours: [],
    })).to.equal('vi/V');

    expect(renderRomanChord({
      type: 'roman',
      root: 'bvii',
      isMajor: true,
      colours: [],
    })).to.equal('bVII');
  });

  it('should render letter chords', () => {
    expect(renderLetterChord({
      type: 'letter',
      root: 'G#',
      isMajor: false,
      slashNote: 'F#',
      colours: [],
    })).to.equal('G#m/F#');
  });
});


describe('transposing scales', () => {

  it('should transpose normal scales properly', () => {
    expect(transposeScale('C', 'D', 'D')).to.equal('E');
  });

  it('should transpose roman scales to interval shift', () => {
    expect(transposeScale('C', 'roman', 'D')).to.equal('+2');
  });
});
