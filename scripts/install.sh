SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)

pushd $ROOT_DIR

mkdir -p logs

pushd dist/server && npm install --only=prod && popd;
