SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)

pushd $ROOT_DIR

mkdir -p dist

pushd ipponokese && npm run build && popd;
pushd server && npm run build && npm run install-deps && popd;
pushd webapp && npm run build:prod;

# Doing this as a separate step as webapp build will currently result in type errors
popd;

echo "Build finished!"
