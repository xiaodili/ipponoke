SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)

pushd $ROOT_DIR

echo "Rsyncing dist..."
rsync --delete --archive --exclude 'dist/server/node_modules' dist eddie@ipponoke.com:/apps/ipponoke/

echo "Rsyncing data..."
rsync --delete --archive --exclude '.git' --exclude 'snippets' --exclude 'readme.md' ../ipponokese-data eddie@ipponoke.com:/apps

echo "Rsyncing scripts..."
rsync --delete --archive scripts eddie@ipponoke.com:/apps/ipponoke/

echo "Deployment finished. Reboot ipponoke service (sudo service ipponoke restart) on instance to complete."
