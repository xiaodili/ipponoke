import * as React from 'react';
import AddSongCard, { GroupByMode } from './AddSongCard';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import theme from '../lib/theme';

import {
  makeStyles
} from '@material-ui/core/styles';

export default {
  title: 'AddSongCard'
};

const useStyles = makeStyles({
  cardContainer: {
    overflow: 'auto',
    maxWidth: '450px',
    lineHeight: 'normal',
  },
});

export const byScale = () => {

  const metadatas: any[] = [{
    title: 'Heart And Soul',
    key: 'C'
  }, {
    title: 'All Good Things',
    artist: 'Nelly Furtado',
    key: 'Am'
  }, {
    title: 'Canon in D',
    key: 'D'
  }, {
    title: 'Under The Bridge',
    artist: 'RHCP',
    containsTimings: true,
    key: 'E'
  }];

  const classes = useStyles({});

  const noOp: () => void = () => null;

  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes.cardContainer}>
        <AddSongCard
          metadatas={metadatas}
          addSong={noOp}
          groupByMode={GroupByMode.GROUP_BY_SCALE}
          addRandomSong={noOp}
          addSongs={noOp}
          selectedSongs={[]} />
      </div>
    </MuiThemeProvider>
  );
};
