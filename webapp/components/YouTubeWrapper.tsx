// Invisible YouTube iframe wrapper to pipe audio in
import * as React from 'react';
import YouTube, { YouTubeProps } from 'react-youtube';

import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core';

const styles = (theme: Theme) => createStyles({
});

interface Props extends WithStyles<typeof styles> {
  videoId: string;
  ref: React.RefObject<YouTubeWrapper>;
}

// From https://developers.google.com/youtube/iframe_api_reference
interface YouTubePlayer {
  playVideo: () => void;
  setPlaybackRate: (playbackRate: number) => number;
  pauseVideo: () => void;
  seekTo: (seconds: number) => void;
  getPlayerState: () => number;
}

export class YouTubeWrapper extends React.Component<Props, {
  start: number,
  loop: boolean,
  playerVars: any,
}> {

  player: YouTubePlayer = null;

  constructor(props: Props) {

    super(props);

    this.state = {
      start: 0,
      loop: false,
      playerVars: {
      }
    }
  }

  onEnd = (event: any) => {

    const {
      start,
      loop
    } = this.state;

    if (loop) {
      event.target.seekTo(start);
      event.target.playVideo();
    }
  };

  onReady = (event: any) => {
    this.player = event.target;
  }

  public pause = () => {
    this.player.pauseVideo();
  }

  public play = () => {
    this.player.playVideo();
  }

  public scrubAndPlay = (start: number) => {

    this.setState({
      start,
      loop: false,
      playerVars: {
        autoplay: 1,
        start,
      }
    });

    this.player.playVideo();
  }

  public scrubAndLoop = (start: number, end?: number) => {

    this.setState({
      start,
      loop: true,
      playerVars: {
        autoplay: 1,
        start,
        end
      }
    });

    this.player.playVideo();
  }

  public changeSpeed = (speed: number) => {

    this.player.setPlaybackRate(speed);
  }

  render() {

    const {
      classes,
      videoId,
    } = this.props;

    const {
      playerVars,
    } = this.state;

    const opts: YouTubeProps['opts'] = {
      width: '0',
      height: '0',
      playerVars,
    };

    return (
      <div>
        <YouTube
          opts={opts}
          onEnd={this.onEnd}
          onReady={this.onReady}
          videoId={videoId} />
      </div>
    );
  }
}

export default withStyles(styles)(YouTubeWrapper);
