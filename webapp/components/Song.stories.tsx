import * as React from 'react';
import Song from './Song';
import NotificationStore from '../stores/NotificationStore';

import {
  makeStyles
} from '@material-ui/core/styles';

export default {
  title: 'Song'
};

const useStyles = makeStyles({
  cardContainer: {
    overflow: 'auto',
    maxWidth: '450px',
    scrollSnapAlign: 'start', /* latest (Chrome 69+) */
    scrollSnapCoordinate: '0% 0%', /* older (Firefox/IE) */
    WebkitScrollSnapCoordinate: '0% 0%', /* older (Safari) */
  },
});

const noOp: () => null = () => null;

const notificationStore = new NotificationStore();

export const withStandardSong = () => {

  const classes = useStyles({});

  const songData = {
    "id": "moon-river",
    "title": "Moon River",
    "metadata": {
      "artist": "Audrey Hepburn",
      "key": "F",
      "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
    },
    "sections": [{
      "name": "Verse 1",
      "metadata": {},
      "lines": [
        "F    Dm     Bb           F          ",
        "Moon River, wider than a mile,  I'm",
        "Bb              F            Gm/E  A7     ",
        "crossing you in style some   day.      Old",
        "Dm    F7         Bb   D#9-5          ",
        "dream-maker, you heartbreaker, Where-",
        "Dm   Dm7    Dm7/B E7      ",
        "ever you're goin’,     I’m",
        "Am7   D7      Gm7   C9  ",
        "goin’    your way."
      ]
    }, {
      "name": "Verse 2",
      "metadata": {},
      "lines": [
        "F    Dm        Bb             F              ",
        "Two  drifters, off to see the world; there's",
        "Bb            F        Gm/E   A7       ",
        "such a lot of world to see.      We're",
        "Dm   Dm/C    Dm/B   Bb        Am7   ",
        "af - ter the same    rainbow's end, ",
        "Bb                   F       ",
        "  waiting ’round the bend — ",
        "Bb               F       ",
        "  my huckleberry friend,",
        "Dm    Gm      C7         F",
        "Moon River,     and     me."
      ]
    }]
  };

  return (
    <div className={classes.cardContainer}>
      <Song
        song={songData}
        notificationStore={notificationStore}
        removeSong={noOp}
        onRef={noOp}
        changeCardFocus={noOp}
        getSongIdx={noOp} />
    </div >
  );
}

export const withYoutubeTime = () => {

  const classes = useStyles({});

  const songData = {
    "id": "moon-river",
    "title": "Moon River",
    "metadata": {
      "artist": "Audrey Hepburn",
      "key": "F",
      "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
    },
    "sections": [{
      "name": "Verse 1",
      "metadata": {
        "youtube-time": 5,
      },
      "lines": [
        "F    Dm     Bb           F          ",
        "Moon River, wider than a mile,  I'm",
        "Bb              F            Gm/E  A7     ",
        "crossing you in style some   day.      Old",
        "Dm    F7         Bb   D#9-5          ",
        "dream-maker, you heartbreaker, Where-",
        "Dm   Dm7    Dm7/B E7      ",
        "ever you're goin’,     I’m",
        "Am7   D7      Gm7   C9  ",
        "goin’    your way."
      ]
    }, {
      "name": "Verse 2",
      "metadata": {
        "youtube-time": 41,
      },
      "lines": [
        "F    Dm        Bb             F              ",
        "Two  drifters, off to see the world; there's",
        "Bb            F        Gm/E   A7       ",
        "such a lot of world to see.      We're",
        "Dm   Dm/C    Dm/B   Bb        Am7   ",
        "af - ter the same    rainbow's end, ",
        "Bb                   F       ",
        "  waiting ’round the bend — ",
        "Bb               F       ",
        "  my huckleberry friend,",
        "Dm    Gm      C7         F",
        "Moon River,     and     me."
      ]
    }]
  };

  return (
    <div className={classes.cardContainer}>
      <Song
        song={songData}
        notificationStore={notificationStore}
        removeSong={noOp}
        onRef={noOp}
        changeCardFocus={noOp}
        getSongIdx={noOp} />
    </div >
  );
}

export const withLongName = () => {

  const classes = useStyles({});

  const songData = {
    "id": "moon-river",
    "title": "Fly me to the Moon River in an extended manner",
    "metadata": {
      "artist": "Audrey Hepburn",
      "key": "F",
      "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
    },
    "sections": [{
      "name": "Verse 1",
      "metadata": {
        "youtube-time": 5,
      },
      "lines": [
        "F    Dm     Bb           F          ",
        "Moon River, wider than a mile,  I'm",
        "Bb              F            Gm/E  A7     ",
        "crossing you in style some   day.      Old",
        "Dm    F7         Bb   D#9-5          ",
        "dream-maker, you heartbreaker, Where-",
        "Dm   Dm7    Dm7/B E7      ",
        "ever you're goin’,     I’m",
        "Am7   D7      Gm7   C9  ",
        "goin’    your way."
      ]
    }]
  };

  return (
    <div className={classes.cardContainer}>
      <Song
        song={songData}
        notificationStore={notificationStore}
        removeSong={noOp}
        onRef={noOp}
        changeCardFocus={noOp}
        getSongIdx={noOp} />
    </div >
  );
}
