import * as React from 'react';
import { IconButton, Theme, createStyles, withStyles, Snackbar, WithStyles } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import NotificationStore from '../stores/NotificationStore';
import { observer } from 'mobx-react';

const styles = (theme: Theme) => ({
  close: {
    height: theme.spacing(4),
    width: theme.spacing(4),
  },
});

interface IProps extends WithStyles<typeof styles> {
  notificationStore: NotificationStore;
}

@observer
class DismissableSnackbar extends React.Component<IProps, {}> {

  constructor(props: IProps) {
    super(props);
  }

  public render() {
    const {
      classes,
      notificationStore,
    } = this.props;

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            horizontal: 'left',
            vertical: 'bottom',
          }}
          open={Boolean(notificationStore.message)}
          onClose={notificationStore.dismissMessage}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          autoHideDuration={5000}
          message={<span id='message-id'>{notificationStore.message}</span>}
          action={[
            <IconButton
              key='close'
              aria-label='Close'
              color='inherit'
              className={classes.close}
              onClick={notificationStore.dismissMessage}
            >
              <Close />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

export default withStyles(styles)(DismissableSnackbar);
