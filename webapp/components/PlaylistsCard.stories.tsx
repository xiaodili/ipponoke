import * as React from 'react';
import PlaylistsCard from './PlaylistsCard';

import {
  makeStyles
} from '@material-ui/core/styles';

export default {
  title: 'PlaylistsCard'
};

const useStyles = makeStyles({
  cardContainer: {
    overflow: 'auto',
    maxWidth: '450px',
  },
});

export const withDefault = () => {

  const classes = useStyles({});

  const addSong = (id: string) => {
    console.log(`Adding song ${id}`);
  };

  const addPlaylist = (id: string) => {
    console.log(`Adding playlist ${id}`);
  };

  const playlists = [{
    name: 'The Standards',
    id: 'the-standards',
    description: 'All the minor and major key standards',
    songs: [{
      id: 'piano-man',
      title: 'Piano Man',
      artist: 'Billy Joel',
    }, {
      id: 'rolling-in-the-deep',
      title: 'Rolling in the Deep',
      artist: 'Adele',
    }]
  }, {
    name: 'Any Major Key Will Tell You',
    id: 'any-major-key-will-tell-you',
    description: 'All the major standards',
    songs: [{
      id: 'piano-man',
      title: 'Piano Man',
      artist: 'Billy Joel',
    }, {
      id: 'deathly',
      title: 'Deathly',
      artist: 'Aimee Mann',
    }]
  }, {
    name: 'It\'s Just A Minor Thing',
    id: 'its-just-a-minor-thing',
    description: 'All the minor standards',
    songs: [{
      id: 'rolling-in-the-deep',
      title: 'Rolling in the Deep',
      artist: 'Adele',
    }, {
      id: 'counting-stars',
      title: 'Counting Stars',
      artist: 'One Republic',
    }]
  }];

  return (
    <div className={classes.cardContainer}>
      <PlaylistsCard
        addPlaylist={addPlaylist}
        addSong={addSong}
        playlists={playlists} />
    </div>
  );
};
