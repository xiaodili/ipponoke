import * as React from 'react';
import {
  createStyles,
  IconButton,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core';

/* import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled'; */
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const styles = (theme: Theme) => createStyles({
});

interface Props extends WithStyles<typeof styles> {
  isScrolling: boolean;
  toggleAutoScroll: () => void;
}

class AutoScroll extends React.Component<Props> {
  render() {
    const {
      isScrolling,
      toggleAutoScroll,
    } = this.props;
    return (
      <IconButton
        size="small"
        onClick={toggleAutoScroll} >
        {isScrolling
          ? <ArrowDownwardIcon
            color='primary' />
          : <ArrowDownwardIcon />}
      </IconButton>
    );
  }
}

export default withStyles(styles)(AutoScroll);
