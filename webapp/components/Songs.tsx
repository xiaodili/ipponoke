import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import Song, { Song as SongElement } from './Song';
import SongStore from '../stores/SongStore';
import NotificationStore from '../stores/NotificationStore';
import AddSongCard from './AddSongCard';
import JamCard from './JamCard';
import OnboardingCard from './OnboardingCard';
import PlaylistsCard from './PlaylistsCard';
import SongScrubber from './SongScrubber';

import {
  joinClasses,
} from '../lib/utils';

import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core';
import withWidth, { WithWidth } from '@material-ui/core/withWidth';


const styles = (theme: Theme) => createStyles({
  songsContainer: {
    height: '100%',
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    alignItems: 'center',
  },
  scrubberContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  songsScroller: {
    '& > div:nth-child(odd) > div': {
      backgroundColor: theme.cardPalette.light,
    },
    '& > div:nth-child(even) > div': {
      backgroundColor: theme.cardPalette.dark,
    },
    height: '100%',
    display: 'flex',
    // if not set, you can scroll past the TopBar - use hidden instead to stop it doubling up with the JamCard scrollbar
    overflowY: 'hidden',
    maxWidth: '100%',
    scrollSnapType: 'x mandatory', // Chrome
    MsScrollSnapType: 'mandatory', // IE/Edge
    WebkitScrollSnapType: 'mandatory', // Safari
    WebkitScrollSnapDestination: '0% 0%',
    WebkitOverflowScrolling: 'touch' // important for iOS
  },
  cardContainer: {
    [theme.breakpoints.down('sm')]: {
      minWidth: '100%',
    },
    overflow: 'auto',
    maxWidth: '100%',
    scrollSnapAlign: 'start', /* latest (Chrome 69+) */
    scrollSnapCoordinate: '0% 0%', /* older (Firefox/IE) */
    WebkitScrollSnapCoordinate: '0% 0%', /* older (Safari) */
  },
  onboardingCardContainer: {
    maxWidth: '400px',
    [theme.breakpoints.up('sm')]: {
      minWidth: '400px',
    },
  },
  menuCardContainer: {
    [theme.breakpoints.up('sm')]: {
      minWidth: '400px',
    },
  },
  songCardContainer: {
    [theme.breakpoints.up('sm')]: {
      minWidth: '450px',
    },
  }
});


// Doing the filtering here for now.
interface Props extends WithWidth, WithStyles<typeof styles> {
  onRef: (r: Songs) => void;
  songStore: SongStore;
  notificationStore: NotificationStore;
}

// https://gomakethings.com/detecting-when-a-visitor-has-stopped-scrolling-with-vanilla-javascript/
let isScrolling: number;

export class Songs extends React.Component<Props, {
  onboardingDismissed: boolean;
}> {

  private songsRef: React.RefObject<HTMLDivElement>;
  public songElements: { [id: string]: SongElement };

  /* public songs; */

  constructor(props: Props) {

    super(props);
    this.songElements = {};
    this.songsRef = React.createRef();
    this.onCirclePressed = this.onCirclePressed.bind(this);
    this.dismissOnboarding = this.dismissOnboarding.bind(this);
    this.updateSongRef = this.updateSongRef.bind(this);
    this.state = {
      onboardingDismissed: false,
      /* onboardingDismissed: true, */
    };
  }

  /* https://stackoverflow.com/a/51082563/1010076 */
  /* componentDidUpdate(prevProps, prevState) {
     
   *   if (this.props) {
   *     console.log(`from songs.tsx`);
   *     Object.entries(this.props).forEach(([key, val]) =>
   *       prevProps[key] !== val && console.log(`Prop '${key}' changed`)
   *     );
   *   }
     
   *   if (this.state) {
   *     Object.entries(this.state).forEach(([key, val]) =>
   *       prevState[key] !== val && console.log(`State '${key}' changed`)
   *     );
   *   }
   * } */

  public onCirclePressed(idx: number) {
    const songsContainer = this.songsRef.current;
    const cardElX = (songsContainer.children[idx].getBoundingClientRect() as DOMRect).x;
    songsContainer.scrollBy(cardElX, 0);
  }

  componentDidMount() {

    this.props.onRef(this);

    window.addEventListener('scroll', () => {
      window.clearTimeout(isScrolling);
      isScrolling = window.setTimeout(() => {

        const songsContainer = this.songsRef.current;

        const songsX = Array.from(songsContainer.children).map((songEl) => {
          return Math.abs((songEl.getBoundingClientRect() as DOMRect).x);
        });

        const focusedCardIndex = songsX.indexOf(Math.min(...songsX));
        this.props.songStore.changeCardFocus(focusedCardIndex);

      }, 10)
    }, true);
  }

  dismissOnboarding() {
    this.setState({
      onboardingDismissed: true,
    });
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  updateSongRef(id: string, song?: SongElement) {
    this.songElements[id] = song;
  }

  render() {

    const {
      onboardingDismissed,
    } = this.state;

    const {
      classes,
      songStore,
      notificationStore,
    } = this.props;

    const {
      addRandomSong,
      addSong,
      addSongs,
      changeCardFocus,
      getSongIdx,
      metadatas,
      songList,
      removeSong,
    } = songStore;

    // 'xs' on the Galaxy Note 4
    /* console.log('width:', width); */

    const renderedCards = songStore.songList.map((song) => {

      return (
        <div
          className={joinClasses(classes.cardContainer, classes.songCardContainer)}
          key={song.id} >
          <Song
            onRef={this.updateSongRef}
            changeCardFocus={changeCardFocus}
            getSongIdx={getSongIdx}
            removeSong={removeSong}
            notificationStore={notificationStore}
            song={song} />
        </div>
      );
    });

    // AddSong
    renderedCards.push(
      <div
        className={joinClasses(classes.cardContainer, classes.menuCardContainer)}
        key={'addsong'} >
        <AddSongCard
          metadatas={metadatas}
          addRandomSong={addRandomSong}
          addSong={addSong}
          addSongs={addSongs}
          selectedSongs={songList} />
      </div>);

    renderedCards.push(
      <div
        className={joinClasses(classes.cardContainer, classes.menuCardContainer)}
        key={'playlists'} >
        <PlaylistsCard
          playlists={songStore.playlists}
          addSong={addSong}
          addPlaylist={songStore.addPlaylist} />
      </div>);

    // JamCard
    renderedCards.push(
      <div
        className={joinClasses(classes.cardContainer, classes.menuCardContainer)}
        key={'jam'} >
        <JamCard
          id={songStore.id}
          jamId={songStore.jamId}
          jams={songStore.jams}
          createJam={songStore.createJam}
          joinJam={songStore.joinJam}
          joiningJamId={songStore.joiningJamId}
          leaveJam={songStore.leaveJam} />
      </div>);

    //Welcome Card
    if (!onboardingDismissed) {
      renderedCards.push(
        <div
          className={joinClasses(classes.cardContainer, classes.onboardingCardContainer)}
          key={'onboarding'} >
          <OnboardingCard
            onRemove={this.dismissOnboarding} />
        </div>);
    }

    //https://www.grapecity.com/en/blogs/using-css-scroll-snap-points
    // Inline style is for FireFox to get around duplicate keys
    // scrollSnapType: 'mandatory', // FireFox
    return (
      <div className={classes.songsContainer}>
        <SongScrubber
          onboardingDismissed={onboardingDismissed}
          onCirclePressed={this.onCirclePressed}
          songStore={songStore} />
        <div
          className={classes.songsScroller}
          ref={this.songsRef}
          style={{ scrollSnapType: 'mandatory' }} >
          {renderedCards}
        </div>
      </div>
    );
  }
}

export default withWidth()(withStyles(styles)(observer(Songs)));
