import * as React from 'react';
import Select from 'react-select';
import { observer } from 'mobx-react';
import {
  createStyles,
  withStyles,
  MenuItem,
  TextField,
  Theme,
  WithStyles,
} from '@material-ui/core';

import KeyProcessor from '../lib/KeyProcessor';

// Styling from: https://material-ui.com/components/autocomplete/
const styles = createStyles({
  selectContainer: {
    flexGrow: 1,
  },
  select: {
  },
  input: {
    display: 'flex',
    padding: 0,
    height: 'auto',
  },
});


const metadatasToOptions = (metadatas: any[]): { label: string, value: string }[] => {

  return metadatas.map(m => {
    return {
      value: m.id,
      label: m.title
    };
  });
};


function inputComponent(props: any) {
  const {
    inputRef,
    ..._props
  } = props;
  return (
    <div ref={inputRef} {..._props} />
  );
}

function Option(props: any) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component='div'
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Control(props: any) {
  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}


const components = {
  Control,
  Option,
  /* Menu,
   * SingleValue,
   * ValueContainer, */
}

interface Props extends WithStyles<typeof styles> {
  theme: Theme;
  addSong: (songId: string) => void;
  metadatas: any[];
  keyProcessor: KeyProcessor;
  selectedSongIds: string[];
}

export class SongSelect extends React.Component<Props, {
  selectedOption: any
}> {

  public selectRef: React.RefObject<Select>;

  constructor(props: Props) {
    super(props);

    this.selectRef = React.createRef();

    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSelectOnKeyDown = this.handleSelectOnKeyDown.bind(this);
  }

  componentDidMount() {
    this.props.keyProcessor.setSongSelect(this);
  }

  componentWillUnmount() {
    this.props.keyProcessor.setSongSelect(null);
  }

  handleSelectOnKeyDown(e: React.KeyboardEvent<HTMLElement>) {

    if (e.keyCode === 27 || e.key === 'Escape') {
      this.selectRef.current.blur();
    }
  }

  handleChange(selectedOption: any) {
    this.props.addSong(selectedOption.value);
    this.selectRef.current.blur();
  }

  render() {

    const {
      classes,
      theme,
      selectedSongIds,
      metadatas,
    } = this.props;

    const selectStyles = {
      input: (base: any) => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    const songIdLookup = new Set(selectedSongIds);

    const options = metadatasToOptions(metadatas.filter(song => !songIdLookup.has(song.id)));

    const {
      selectedOption
    } = this.state;


    return (
      <div className={classes.selectContainer}>
        <Select
          ref={this.selectRef}
          onKeyDown={this.handleSelectOnKeyDown}
          styles={selectStyles}
          classes={classes}
          placeholder={'Search'}
          className={classes.select}
          components={components}
          value={selectedOption}
          onChange={this.handleChange}
          options={options}
        />
      </div>
    );

  }
}

export default withStyles(styles, { withTheme: true })(observer(SongSelect));
