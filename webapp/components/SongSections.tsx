import * as React from 'react';

import { observer } from 'mobx-react';

import {
  createStyles,
  IconButton,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import RepeatIcon from '@material-ui/icons/Repeat';
import PauseIcon from '@material-ui/icons/Pause';

import {
  parseDuration,
  isWhitespace,
  groupChordLyricLines,
  transposeGroupedLine,
} from '../lib/utils';

import {
  chordRomanToLetter,
  chordLetterToRoman,
  parseChord,
  transposeScale,
  renderRomanChord,
  renderLetterChord,
} from '../../ipponokese/notation';

const styles = (theme: Theme) => createStyles({
  annotatedLine: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chordCell: {
    color: theme.chordPalette
      ? theme.chordPalette.chordLine
      : 'blue', // Exposed for Storybook
  },
  sectionTitleContainer: {
    display: 'flex',
    alignItems: 'space-end',
    justifyContent: 'space-between',
  },
  lyricCell: {
  },
  sectionContainer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  vcell: {
  }
});

// start/end 2 seconds before/after
const SECTION_START_TOLERANCE = 2;
const SECTION_END_TOLERANCE = 2;

const replaceChords = (
  line: string,
  oldChordStrs: string[],
  newChordStrs: string[],
  chordStartIdxs: number[]): string => {

  const newLine: string[] = line.split('');

  for (let i = 0; i < chordStartIdxs.length - 1; i++) { // Do up to the last one:

    const oldChord = oldChordStrs[i];

    for (let j = 0; j < oldChord.length; j++) {
      newLine[chordStartIdxs[i] + j] = ' ';
    }

    const newChord = newChordStrs[i];

    for (let j = 0; j < newChord.length; j++) {
      newLine[chordStartIdxs[i] + j] = newChord[j];
    }
  }

  return newLine.slice(0, chordStartIdxs[chordStartIdxs.length - 1]).join('') + newChordStrs[newChordStrs.length - 1];
};

const makeChordLine = (
  currentScale: string,
  originalKey: string,
  line: string): string => {

  const chordStartIdxs = [];
  const chordStrs: string[] = [];

  var traversingWord = false;
  let startWordIdx: number;

  // For-loops rock!!!
  for (let i = 0; i < line.length; i++) {

    if (!isWhitespace(line[i])) {
      if (!traversingWord) {
        chordStartIdxs.push(i);
        startWordIdx = i;
      }
      traversingWord = true;
    } else {
      if (traversingWord) {
        chordStrs.push(line.slice(startWordIdx, i));
      }
      traversingWord = false;
    }
  }

  if (traversingWord) {
    chordStrs.push(line.slice(startWordIdx, line.length));
  }

  const chords = chordStrs.map(chordStr => parseChord(chordStr));

  if (chords[0].type === 'roman') { //Originally penned in roman numerals (hardcore!)
    if (currentScale === 'roman') {
      return line; //no-op
    } else { //convert roman to letter

      const chordLetters = chords.map(chord => chordRomanToLetter(chord, currentScale));

      const newChordStrs = chordLetters.map(chord => renderLetterChord(chord));
      return replaceChords(line, chordStrs, newChordStrs, chordStartIdxs);
    }
  } else { // letter
    if (originalKey === currentScale) {
      return line; //no-op
    } else {
      const chordNumerals = chords.map(chord => chordLetterToRoman(chord, originalKey));

      if (currentScale === 'roman') { //render roman
        const newChordStrs = chordNumerals.map(chord => renderRomanChord(chord));
        return replaceChords(line, chordStrs, newChordStrs, chordStartIdxs);
      } else { //render a new key

        const chordLetters = chordNumerals.map(chord => chordRomanToLetter(chord, currentScale));
        const newChordStrs = chordLetters.map(chord => renderLetterChord(chord));
        return replaceChords(line, chordStrs, newChordStrs, chordStartIdxs);
      }
    }
  }
}

interface Props extends WithStyles<typeof styles> {
  sections: any[];
  currentScale: string;
  originalKey: string;
  playSection: (seconds: number) => void;
  pause: () => void;
  playSectionLoop: (start: number, end?: number) => void;
}

export class SongSections extends React.Component<Props, {}> {

  onSectionLooped = (idx: number) => {

    const {
      sections,
      playSectionLoop,
    } = this.props;

    const start: number = parseDuration(sections[idx].metadata['youtube-time']);

    if (sections.length - 1 === idx) { // Last section

      playSectionLoop(Math.max(0, start - SECTION_START_TOLERANCE));

    } else {

      const end: number = parseDuration(sections[idx + 1].metadata['youtube-time']);

      // Is it fine if end is out of bounds?
      playSectionLoop(Math.max(0, start - SECTION_START_TOLERANCE), end + SECTION_END_TOLERANCE);
    }
  }

  render() {

    const {
      classes,
      sections,
      currentScale,
      originalKey,
      playSection,
    } = this.props;

    const renderSectionMetadata = (metadata: any) => {

      const {
        key,
      } = metadata;

      let scale;

      if (key) {
        scale = originalKey === currentScale
          ? key
          : transposeScale(originalKey, currentScale, key);
      }

      return (
        <div>
          {key && <div>
            {`key: ${scale}`}
          </div>}
        </div>
      );
    }

    const renderAnnotatedLine = (annotatedLine: any[], lidx: number) => {

      // Cell denotes a vertical slice of a line
      return (<div className={classes.annotatedLine} key={lidx}>
        {annotatedLine.map((cell: any, i: number) => {
          return (<div className={classes.vcell} key={i}>
            {cell.chord && <div className={classes.chordCell}>
              {cell.chord}
            </div>}
            {cell.lyric && <div className={classes.lyricCell}>
              {cell.lyric}
            </div>}
          </div>)
        })}
      </div>);
    }

    const renderSection = (section: any, sectionIdx: number) => {

      const key = section.metadata.key ? section.metadata.key : originalKey;

      let scale = section.metadata.key && currentScale !== 'roman'
        ? transposeScale(originalKey, currentScale, section.metadata.key)
        : currentScale;

      // TODO: Make this key change section transposition logic more straightforward
      const transformChord = (line: string) => {
        return makeChordLine(scale, currentScale !== originalKey ? key : scale, line);
      };

      const annotatedLines = groupChordLyricLines(section.lines, transformChord).map((groupedLine: any, lidx: number) => {

        const annotatedLine = transposeGroupedLine(groupedLine);

        return renderAnnotatedLine(annotatedLine, lidx);
      })

      return (
        <div className={classes.sectionContainer} key={sectionIdx}>
          <Typography variant='subtitle1' gutterBottom>
            <div className={classes.sectionTitleContainer}>
              {section.name}
              {section.metadata['youtube-time'] &&
                <span>
                  <IconButton
                    size="small"
                    onClick={() => playSection(
                      parseDuration(section.metadata['youtube-time']))}>
                    <PlayCircleFilledIcon />
                  </IconButton>
                  <IconButton
                    size="small"
                    onClick={() => this.onSectionLooped(sectionIdx)}>
                    <RepeatIcon />
                  </IconButton>
                  <IconButton
                    size="small"
                    onClick={this.props.pause}>
                    <PauseIcon />
                  </IconButton>
                </span>
              }
            </div>
          </Typography>
          {renderSectionMetadata(section.metadata)}
          {annotatedLines}
        </div>
      );
    }

    return (
      <div>
        {
          sections.map((section: any, idx: number) => {
            return renderSection(section, idx);
          })
        }
      </div >
    );
  }
}

export default withStyles(styles)(observer(SongSections));
