import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import {
  Button,
  Card,
  CardContent,
  createStyles,
  List,
  ListItemSecondaryAction,
  ListItem,
  ListItemText,
  MenuItem,
  Select,
  withStyles,
  WithStyles,
  Tooltip,
  Theme,
  Typography,
} from '@material-ui/core';

import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';

import {
  majorMinorEnharmonics,
} from '../../ipponokese/notation';

const majMinScales: string[] = _.flatten(majorMinorEnharmonics);

const styles = (theme: Theme) => createStyles({
  addSongBtn: {
    marginRight: theme.spacing(1),
  },
  addContainer: {
    marginRight: theme.spacing(1),
  },
  addTitleContainer: {
    display: 'flex',
  },
  addActionContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  button: {
    marginRight: theme.spacing(1),
  },
  headingContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingBottom: theme.spacing(1),
  },
  songContainer: {
    overflow: 'auto',
    height: '100%',
    boxSizing: 'content-box',
  },
  songsNested: {
    paddingLeft: theme.spacing(2),
  },
  songsList: {
    backgroundColor: theme.palette.background.paper,
  }
});

export enum GroupByMode {
  GROUP_BY_ARTIST = 'by artist',
  GROUP_BY_SCALE = 'by scale',
}

interface Props extends WithStyles<typeof styles> {
  metadatas: any[];
  addSong: (id: string) => void;
  addRandomSong: () => void;
  groupByMode?: GroupByMode;
  addSongs: (ids: string[]) => void;
  selectedSongs: any[];
}

@observer
class AddSongCard extends React.Component<Props, {
  groupByMode: GroupByMode
}> {

  constructor(props: Props) {
    super(props);

    this.handleGroupModeChange = this.handleGroupModeChange.bind(this);

    this.state = {
      groupByMode: props.groupByMode || GroupByMode.GROUP_BY_ARTIST
    };
  }

  /* https://stackoverflow.com/a/51082563/1010076 */
  /* componentDidUpdate(prevProps, prevState) {

   *   if (this.props) {
   *     Object.entries(this.props).forEach(([key, val]) =>
   *       prevProps[key] !== val && console.log(`Prop '${key}' changed`)
   *     );
   *   }

   *   if (this.state) {
   *     Object.entries(this.state).forEach(([key, val]) =>
   *       prevState[key] !== val && console.log(`State '${key}' changed`)
   *     );
   *   }
   * } */

  handleGroupModeChange(evt: React.ChangeEvent<HTMLSelectElement>) {

    const newMode = evt.target.value as GroupByMode;

    if (newMode !== this.state.groupByMode) {
      this.setState({
        groupByMode: newMode,
      });
    }
  }

  render() {
    const {
      addSong,
      addRandomSong,
      addSongs,
      classes,
      metadatas,
      selectedSongs,
    } = this.props;

    const {
      groupByMode,
    } = this.state;

    const songIdLookup = new Set(selectedSongs.map(s => s.id));

    const onClick = (songId: string) => () => {
      addSong(songId);
    }


    const renderAddAction = (song: any) => {

      const alreadyAdded = songIdLookup.has(song.id);

      return (
        <Button
          color='primary'
          disabled={alreadyAdded}
          className={classes.addSongBtn}
          onClick={onClick(song.id)}
          variant='contained'
          size='small'>
          {alreadyAdded ? 'Added' : 'Add'}
        </Button>
      );
    };

    const renderAddAllAction = (songs: any[]) => {

      const notAdded = songs.filter(song => !songIdLookup.has(song.id));
      const allAdded = notAdded.length === 0;

      return (
        <Button
          disabled={allAdded}
          color='primary'
          className={classes.addSongBtn}
          onClick={() => addSongs(notAdded.map(s => s.id))}
          variant='contained'
          size='small'>
          {allAdded ? 'All Added' : 'Add All'}
        </Button>
      );

    };

    const renderSong = (song: any, idx: number) => {
      return (
        <ListItem
          divider
          dense
          key={idx}>
          {song.containsTimings &&
            <Tooltip title="Song playback timings available">
              <FormatListBulletedIcon
                fontSize="small"
                color="disabled" />
            </Tooltip>}
          <ListItemText
            primary={song.title}
            secondary={groupByMode === GroupByMode.GROUP_BY_ARTIST ? null : song.artist}
          />
          <ListItemSecondaryAction>
            <div className={classes.addActionContainer}>
              {renderAddAction(song)}
            </div>
          </ListItemSecondaryAction>
        </ListItem >
      );
    };

    const renderSongs = () => {

      if (groupByMode === GroupByMode.GROUP_BY_ARTIST) {

        const byArtist = _.toPairs(_.groupBy(metadatas, 'artist'));
        return byArtist.map((artistSongs) => {

          const [
            artist,
            songs
          ] = artistSongs;

          return (
            <div key={artist}>
              <ListItem
                divider >
                <ListItemText
                  primary={artist} />
                <ListItemSecondaryAction>
                  {renderAddAllAction(songs)}
                </ListItemSecondaryAction>
              </ListItem>
              <List
                dense
                className={classes.songsNested}
                disablePadding>
                {songs.map(renderSong)}
              </List>
            </div>
          );
        });
      } else { // GROUP_BY_SCALE

        const byScale = _.groupBy(metadatas, (song) => {
          const keyIndex = _.flatten(majorMinorEnharmonics).findIndex((key: string) => song.key === key);
          return majMinScales[keyIndex];
        });

        const byScaleSortedMusically = majMinScales.map((scale: string) => {
          return {
            scale,
            songs: (scale in byScale) ? byScale[scale] : []
          };
        })

        return byScaleSortedMusically.map((scaleSongs) => {

          const {
            scale,
            songs
          } = scaleSongs;

          return (
            <div key={scale}>
              <ListItem
                divider >
                <ListItemText
                  primary={scale} />
                <ListItemSecondaryAction>
                  {renderAddAllAction(songs)}
                </ListItemSecondaryAction>
              </ListItem>
              <List
                dense
                className={classes.songsNested}
                disablePadding>
                {songs.map(renderSong)}
              </List>
            </div>
          );
        });
      }
    };

    return (
      <Card className={classes.songContainer} >
        <CardContent>
          <div className={classes.headingContainer}>
            <div className={classes.addTitleContainer}>
              <Typography
                className={classes.addContainer}
                variant='h6'>Songs</Typography>
              <Select
                value={this.state.groupByMode}
                onChange={this.handleGroupModeChange}
              >
                <MenuItem value={GroupByMode.GROUP_BY_ARTIST}>by artist</MenuItem>
                <MenuItem value={GroupByMode.GROUP_BY_SCALE}>by scale</MenuItem>
              </Select>
            </div>
            <Button
              color='primary'
              onClick={addRandomSong}
              size='small'
              variant='contained'>
              Random
            </Button>
          </div>
          <List
            className={classes.songsList}
          >
            {renderSongs()}
          </List>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(AddSongCard);
