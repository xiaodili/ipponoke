import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import {
  Card,
  CardContent,
  createStyles,
  List,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import PlaylistSection from './PlaylistSection';

const styles = (theme: Theme) => createStyles({
  joinButton: {
    marginRight: theme.spacing(1),
  },
  playlistsContainer: {
    background: theme.palette.background.paper,
  },
  cardContainer: {
    overflow: 'auto',
    height: '100%',
    boxSizing: 'content-box',
  },
});

interface Props extends WithStyles<typeof styles> {
  playlists: any[];
  addPlaylist: (playlistId: string) => void;
  addSong: (songId: string) => void;
}

@observer
class PlaylistsCard extends React.Component<Props, {}> {

  render() {

    const {
      classes,
      playlists,
      addPlaylist,
      addSong,
    } = this.props;

    return (
      <Card className={classes.cardContainer} >
        <CardContent>
          <Typography variant='h6'>Set Lists</Typography>
          <List className={classes.playlistsContainer}>
            {playlists.map((playlist: any) =>
              <PlaylistSection
                key={playlist.id}
                name={playlist.name}
                songs={playlist.songs}
                onAddPlaylist={() => addPlaylist(playlist.id)}
                addSong={addSong}
              />)}
          </List>
        </CardContent>
      </Card >
    );
  }
}

export default withStyles(styles)(PlaylistsCard);
