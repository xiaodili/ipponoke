import * as React from 'react';
import PlaybackRateSelector from './PlaybackRateSelector';

export default {
  title: 'PlaybackRateSelector'
};

export const withDefault = () => {

  const noOp: () => void = () => {
    console.log(`hi`);
  };

  return (
    <PlaybackRateSelector
      onQuarterPressed={noOp}
      onHalfPressed={noOp}
      onThreeQuartersPressed={noOp}
      onFullPressed={noOp} />
  );
};
