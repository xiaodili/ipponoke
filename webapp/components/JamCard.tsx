import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import {
  Button,
  Card,
  CardContent,
  createStyles,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

const styles = (theme: Theme) => createStyles({
  selected: {
    backgroundColor: `${theme.palette.primary.light} !important`,
  },
  joinButton: {
    marginRight: theme.spacing(1),
  },
  jamList: {
    background: theme.palette.background.paper,
  },
  songContainer: {
    overflow: 'auto',
    height: '100%',
    boxSizing: 'content-box',
  },
});

interface Props extends WithStyles<typeof styles> {
  id?: string;
  jamId?: string;
  joiningJamId?: string;
  jams: any[];
  createJam: () => void;
  joinJam: (jamId: string) => void;
  leaveJam: () => void;
}

@observer
class JamCard extends React.Component<Props, {
}> {

  constructor(props: Props) {
    super(props);

  }

  /* https://stackoverflow.com/a/51082563/1010076 */
  /* componentDidUpdate(prevProps, prevState) { */
  /* console.log('prevProps:', prevProps);
   * console.log('prevState:', prevState); */


  /* Object.entries(this.props).forEach(([key, val]) =>
   *   prevProps[key] !== val && console.log(`Prop '${key}' changed`)
   * ); */
  /* Object.entries(this.state).forEach(([key, val]) =>
   *   prevState[key] !== val && console.log(`State '${key}' changed`)
   * ); */
  /* } */

  render() {
    const {
      classes,
      jams,
      id,
      jamId,
      createJam,
      joiningJamId,
      joinJam,
      leaveJam,
    } = this.props;

    const renderAction = (_jamId: string) => {

      if (joiningJamId === _jamId) {

        return (
          <Button
            color='primary'
            variant='contained'
            size='small'
            className={classes.joinButton}
            disabled>
            Joining
          </Button >
        );

      } else if (!joiningJamId && !jamId) {

        return (
          <Button
            color='primary'
            variant='contained'
            size='small'
            className={classes.joinButton}
            onClick={() => joinJam(_jamId)}>
            Join
          </Button>);

      } else if (jamId === _jamId) {

        return (
          <Button
            color='primary'
            variant='contained'
            size='small'
            className={classes.joinButton}
            onClick={() => leaveJam()}>
            Leave
          </Button>
        );

      } else {

        return null;
      }
    };

    const renderedJams = jams.map((jam: any, i: number) => {

      return (
        <ListItem
          divider
          dense
          key={i}
          classes={{ selected: classes.selected }}
          selected={jamId && jamId === jam.id}
        >
          <ListItemText
            disableTypography={true}
            primary={<Typography variant='subtitle2'>{jam.name}</Typography>}
            secondary={<div>
              <Typography variant='caption'>
                Number of participants: {jam.participants.length}
              </Typography><br />
              <Typography variant='caption'>
                Number of songs: {jam.songs.length}
              </Typography>
            </div>
            }
          />
          <ListItemSecondaryAction>
            {renderAction(jam.id)}
          </ListItemSecondaryAction>
        </ListItem>
      );
    });

    return (
      <Card className={classes.songContainer} >
        <CardContent>
          <Typography variant='h6'>Jam Rooms</Typography>
          <List className={classes.jamList}>
            {renderedJams}
          </List>
        </CardContent>
      </Card >
    );
  }
}

export default withStyles(styles)(JamCard);
