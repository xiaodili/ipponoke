import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import YouTubeWrapper, { YouTubeWrapper as YouTubeWrapperElement } from './YouTubeWrapper';

import PlaybackRateSelector from './PlaybackRateSelector';

import {
  Button,
  Card,
  CardContent,
  createStyles,
  FormHelperText,
  IconButton,
  withStyles,
  WithStyles,
  Theme,
  Tooltip,
  Typography,
} from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import MaximizeIcon from '@material-ui/icons/Maximize';
import ShareIcon from '@material-ui/icons/Share';
/* import OpenInNewIcon from '@material-ui/icons/OpenInNew'; */
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import RepeatIcon from '@material-ui/icons/Repeat';
import PauseIcon from '@material-ui/icons/Pause';

import NotificationStore from '../stores/NotificationStore';

import AutoScroll from './AutoScroll';
import ScaleSelector from './ScaleSelector';
import SongSections from './SongSections';
import SongMaximised from './SongMaximised';

const styles = (theme: Theme) => createStyles({
  zoomIconContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  cardContainer: {
    backgroundColor: 'inherit',
  },
  playbackOptionsContainer: {
    marginLeft: theme.spacing(1)
  },
  playIconContainer: {
    minWidth: '80px',
    justifyContent: 'center',
    display: 'flex',
    marginTop: theme.spacing(1) / 2,
  },
  durationContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  zoomContainer: {
  },
  inlineOptionsContainer: {
    backgroundColor: 'inherit',
    position: '-webkit-sticky',
    top: '0px',
    display: 'flex',
    justifyContent: 'space-between'
  },
  titleContainer: {
    marginBottom: theme.spacing(1.5)
  },
  optionsContainer: {
    display: 'flex',
    alignItems: 'space-end',
    justifyContent: 'space-between',
  },
  preformattedContainer: {
    fontFamily: 'monospace',
    whiteSpace: 'pre-wrap',
  },
  sectionContainer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  songContainer: {
    overflow: 'auto',
    width: '100%',
    height: '100%',
    boxSizing: 'border-box',
  },
  songTitleContainer: {
    display: 'flex'
  },
  songHeaderContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export interface SongProps extends WithStyles<typeof styles> {
  song: any;
  removeSong: (id: string) => void;
  onRef: (id: string, r: Song) => void;
  changeCardFocus: (idx: number) => void;
  getSongIdx: (id: string) => number;
  notificationStore: NotificationStore;
}

const defaultFontSize = 13; //in pixels

// Keep scrollUpdatePeriod/defaultScrollSpeed at ~10
const scrollUpdatePeriod = 160;
const defaultScrollSpeed = 16;

const scrollSpeedFactor = 10;

let autoScrolling: number;

export class Song extends React.Component<SongProps, {
  currentScale: string,
  originalKey: string,
  fontSize: number,
  maximised: boolean,
  scrollSpeed: number,
  autoScrolling: boolean,
  autoScrollClicked: boolean,
}> {

  private songRef: React.RefObject<HTMLDivElement>;
  private youTubeWrapper: React.RefObject<YouTubeWrapperElement>;

  constructor(props: SongProps) {
    super(props);

    this.songRef = React.createRef();
    this.youTubeWrapper = React.createRef();

    this.state = {
      autoScrollClicked: false,
      autoScrolling: false,
      maximised: false,
      scrollSpeed: defaultScrollSpeed,
      currentScale: props.song.metadata.key,
      originalKey: props.song.metadata.key,
      fontSize: defaultFontSize,
    };

    this.toggleAutoScroll = this.toggleAutoScroll.bind(this);
    this.startAutoScroll = this.startAutoScroll.bind(this);
    this.stopAutoScroll = this.stopAutoScroll.bind(this);
    this.fastScroll = this.fastScroll.bind(this);
    this.slowScroll = this.slowScroll.bind(this);
    this.goToTop = this.goToTop.bind(this);
    this.maximiseSong = this.maximiseSong.bind(this);
    this.copySongLink = this.copySongLink.bind(this);
    this.minimiseSong = this.minimiseSong.bind(this);
    this.goToBottom = this.goToBottom.bind(this);
    this.openYouTube = this.openYouTube.bind(this);
    this.toggleMaximise = this.toggleMaximise.bind(this);
  }

  componentDidMount() {
    this.props.onRef(this.props.song.id, this);
  }

  componentWillUnmount() {
    this.props.onRef(this.props.song.id, undefined);
  }

  /* https://stackoverflow.com/a/51082563/1010076 */
  /* componentDidUpdate(prevProps, prevState) {

   *   if (this.props) {
   *     console.log(`from song.tsx`);
   *     Object.entries(this.props).forEach(([key, val]) =>
   *       prevProps[key] !== val && console.log(`Prop '${key}' changed`)
   *     );
   *   }

   *   if (this.state) {
   *     Object.entries(this.state).forEach(([key, val]) =>
   *       prevState[key] !== val && console.log(`State '${key}' changed`)
   *     );
   *   }
   * } */

  onPlaybackChangePressed = (speed: number) => {

    if (this.youTubeWrapper.current) {
      this.youTubeWrapper.current.changeSpeed(speed);
    }
  }

  private onScaleChanged = (newScale: string) => {

    this.setState({
      currentScale: newScale,
    });
  }

  private onTextSizeDefault = () => {

    this.setState({
      fontSize: defaultFontSize,
    });
  }

  private onTextSizeChanged = (zoomIn: boolean) => () => {

    this.setState({
      fontSize: zoomIn
        ? this.state.fontSize + 1
        : Math.max(5, this.state.fontSize - 1)
    });
  }

  private stopAutoScroll() {

    window.clearInterval(autoScrolling);

    this.setState({
      autoScrolling: false,
      autoScrollClicked: true,
    });
  }

  private startAutoScroll() {

    autoScrolling = window.setInterval(() => {

      // This needs to be greater than 1 to register
      const amountToScroll = this.state.scrollSpeed / scrollSpeedFactor;
      this.songRef.current.scrollBy(0, amountToScroll);

      if (this.songRef.current.scrollTop + this.songRef.current.clientHeight >= this.songRef.current.scrollHeight) {
        window.clearInterval(autoScrolling);
        this.setState({
          autoScrolling: false
        });
      }
    }, scrollUpdatePeriod);

    this.setState({
      autoScrolling: true,
      autoScrollClicked: true,
    });
  }

  public toggleAutoScroll() {

    if (!this.state.autoScrolling) {
      this.startAutoScroll();
    } else {
      this.stopAutoScroll();
    }
  }

  public openYouTube() {

    const link = this.props.song.metadata.link;

    if (link) {
      window.open(link, '_blank');
    }
  }

  public goToTop() {
    this.songRef.current.scrollTo({
      left: 0,
      top: 0,
    });
  }

  public goToBottom() {
    this.songRef.current.scrollTo({
      left: 0,
      top: this.songRef.current.scrollHeight,
      /* behavior: 'smooth', */ // Too slow
    });
  }

  public fastScroll() {

    this.setState({
      scrollSpeed: this.state.scrollSpeed + 2,
    });
  }

  public slowScroll() {

    this.setState({
      scrollSpeed: Math.max(0, this.state.scrollSpeed - 2),
    });
  }

  public copySongLink() {
    // origin seems to be href without the / e.g. http:localhost:9200
    const songLink = window.location.origin + "#songs/" + this.props.song.id;
    navigator.clipboard.writeText(songLink);
    this.props.notificationStore.setMessage("Song link copied to clipboard");
  }

  public maximiseSong() {

    const {
      changeCardFocus,
      getSongIdx,
      song,
    } = this.props;

    changeCardFocus(getSongIdx(song.id));

    this.setState({
      maximised: true,
    });
  }

  public minimiseSong() {

    this.setState({
      maximised: false,
    });
  }

  public toggleMaximise() {

    if (this.state.maximised) {
      this.minimiseSong();
    } else {
      this.maximiseSong();
    }
  }

  pause = () => {
    if (this.youTubeWrapper.current) {
      this.youTubeWrapper.current.pause();
    }
  }

  playSection = (start: number) => {

    if (this.youTubeWrapper.current) {
      // https://developers.google.com/youtube/iframe_api_reference#onStateChange
      this.youTubeWrapper.current.scrubAndPlay(start);
    }
  }

  playSectionLoop = (start: number, end?: number) => {

    if (this.youTubeWrapper.current) {
      this.youTubeWrapper.current.scrubAndLoop(start, end);
    }
  }

  render() {
    const {
      classes,
      removeSong,
      song,
    } = this.props;

    const {
      fontSize,
      currentScale,
      originalKey,
      maximised,
      autoScrollClicked,
      autoScrolling,
      scrollSpeed,
    } = this.state;

    const containsYouTubeLink = song.metadata && song.metadata.link;

    const renderSongMetadata = (metadata: any) => {

      const {
        artist,
      } = metadata;

      return (
        <div className={classes.titleContainer}>
          {artist &&
            <Typography
              variant='caption' >
              {`by ${artist}`}
            </Typography>}
        </div>
      );
    }

    const renderedSong = maximised

      ? <SongMaximised
        song={song}
        currentScale={currentScale}
        fontSize={fontSize}
        originalKey={originalKey}
        onPlaybackChangePressed={this.onPlaybackChangePressed}
        minimiseSong={this.minimiseSong}
        playSection={this.playSection}
        playSectionLoop={this.playSectionLoop}
        pause={this.pause}
        removeSong={() => removeSong(song.id)} />

      :

      <Card className={classes.songContainer} ref={this.songRef}>
        <CardContent className={classes.cardContainer}>
          <div className={classes.songHeaderContainer}>

            <div className={classes.songTitleContainer}>
              <Typography variant='h6'>
                {song.title}
              </Typography>
              <div
                className={classes.playbackOptionsContainer}>
                <Tooltip title='Play audio from YouTube'>
                  <IconButton
                    size="small"
                    onClick={() => this.playSection(0)}>
                    <PlayCircleFilledIcon />
                  </IconButton>
                </Tooltip>
                {/* <Tooltip title='Open YouTube link'>
                    <IconButton onClick={this.openYouTube}>
                    <OpenInNewIcon />
                    </IconButton>
                    </Tooltip> */}
                <Tooltip title='Loop audio from YouTube'>
                  <IconButton
                    size="small"
                    onClick={() => this.playSectionLoop(0)}>
                    <RepeatIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title='Pause YouTube audio'>
                  <IconButton
                    size="small"
                    onClick={this.pause}>
                    <PauseIcon />
                  </IconButton>
                </Tooltip>
              </div>
            </div>

            <div>
              <Tooltip title="Copy song link">
                <IconButton
                  size="small"
                  onClick={this.copySongLink}>
                  <ShareIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Maximise song view">
                <IconButton
                  size="small"
                  onClick={this.maximiseSong}>
                  <MaximizeIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Remove song">
                <IconButton
                  size="small"
                  onClick={() => removeSong(song.id)}>
                  <CloseIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          {renderSongMetadata(song.metadata)}
          <div className={classes.optionsContainer}>
            <ScaleSelector
              scale={currentScale}
              originalKey={originalKey}
              onScaleChanged={this.onScaleChanged}
            />
            {containsYouTubeLink && <PlaybackRateSelector
              onQuarterPressed={() => this.onPlaybackChangePressed(0.25)}
              onHalfPressed={() => this.onPlaybackChangePressed(0.5)}
              onThreeQuartersPressed={() => this.onPlaybackChangePressed(0.75)}
              onFullPressed={() => this.onPlaybackChangePressed(1)}
            />}
          </div>
          <div className={classes.inlineOptionsContainer} style={{
            position: 'sticky'
          }}>
            <div className={classes.zoomContainer}>
              <FormHelperText>Zoom</FormHelperText>
              <div className={classes.zoomIconContainer}>
                <IconButton
                  size="small"
                  onClick={this.onTextSizeChanged(false)} >
                  <ZoomOutIcon />
                </IconButton>
                <Button onClick={this.onTextSizeDefault}>Default</Button>
                <IconButton
                  size="small"
                  onClick={this.onTextSizeChanged(true)} >
                  <ZoomInIcon />
                </IconButton>
              </div>
            </div>
            <div className={classes.durationContainer}>
              <FormHelperText>
                {scrollSpeed === defaultScrollSpeed
                  ? 'Autoscroll'
                  : `Autoscroll (${scrollSpeed / defaultScrollSpeed}x)`}
              </FormHelperText>
              <div className={classes.playIconContainer}>
                {autoScrollClicked &&
                  <IconButton
                    size="small"
                    onClick={this.slowScroll} >
                    <RemoveIcon />
                  </IconButton>
                }
                <AutoScroll
                  isScrolling={autoScrolling}
                  toggleAutoScroll={this.toggleAutoScroll}
                />
                {autoScrollClicked &&
                  <IconButton onClick={this.fastScroll} >
                    <AddIcon />
                  </IconButton>
                }
              </div>
            </div>
          </div>
          <div className={classes.preformattedContainer} style={{
            fontSize: `${fontSize}px`
          }}>
            <SongSections
              sections={song.sections}
              currentScale={currentScale}
              originalKey={originalKey}
              playSection={this.playSection}
              pause={this.pause}
              playSectionLoop={this.playSectionLoop}
            />
          </div>
        </CardContent>
      </Card >;
    return (
      <div>
        {renderedSong}
        {containsYouTubeLink &&
          <YouTubeWrapper
            ref={this.youTubeWrapper}
            videoId={song.metadata.link.split('=').pop()} />}
      </div>
    );
  }
}

export default withStyles(styles)(observer(Song));
