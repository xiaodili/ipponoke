import * as React from 'react';
import {
  createStyles,
  FormControl,
  Input,
  InputLabel,
  Select,
  MenuItem,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core';

import {
  validMajorScales,
  validMinorScales,
  parseScale,
} from '../../ipponokese/notation';

const styles = (theme: Theme) => createStyles({
  formControl: {
  }
});

interface Props extends WithStyles<typeof styles> {
  originalKey: string,
  scale: string;
  onScaleChanged: (scale: string) => void;
}

class ScaleSelector extends React.Component<Props> {

  constructor(props: Props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: any) {
    this.props.onScaleChanged(event.target.value);
  }

  render() {

    const {
      scale,
      originalKey,
      classes
    } = this.props;

    const {
      isMajor
    } = parseScale(originalKey);

    let keys;

    if (isMajor) {
      keys = validMajorScales;
    } else {
      keys = validMinorScales.map(s => `${s}m`);
    }

    const renderMenuItem = (key: string, idx: number) => {
      return (
        <MenuItem
          key={idx}
          value={key}>{`${key}${key === originalKey ? ' (original)' : ''}`}
        </MenuItem>
      );
    };
    return (
      <FormControl className={classes.formControl}>
        <InputLabel>Key</InputLabel>
        <Select
          value={scale}
          onChange={this.handleChange}
          input={<Input />}
        >
          <MenuItem value='roman'>Numeral</MenuItem>
          {keys.map((key, idx) => renderMenuItem(key, idx))}
        </Select>
      </FormControl>
    );
  }
}
export default withStyles(styles)(ScaleSelector);
