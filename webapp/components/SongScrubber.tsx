// Not using this yet
import * as React from 'react';
import { observer } from 'mobx-react';
import SongStore from '../stores/SongStore';
import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core';

const styles = (theme: Theme) => createStyles({
  circleContainer: {
    minHeight: '30px',
    display: 'flex',
    alignItems: 'center',
  },
  circle: {
    borderRadius: '50%',
    cursor: 'pointer',
    height: '15px',
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    width: '15px',
    backgroundColor: 'grey',
  },
  circleSelected: {
  },
});


interface Props extends WithStyles<typeof styles> {
  onboardingDismissed: boolean;
  onCirclePressed: (idx: number) => void;
  theme: Theme;
  songStore: SongStore;
}

@observer
class SongScrubber extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
  }

  onCirclePress = (idx: number) => () => {
    this.props.songStore.changeCardFocus(idx);
    this.props.onCirclePressed(idx);
  }

  render() {
    const {
      classes,
      onboardingDismissed,
      songStore,
      theme,
    } = this.props;

    const {
      songList,
      currentCardFocus,
    } = songStore;

    const dummyCards: any[] = [null, null, null];

    if (!onboardingDismissed) {
      dummyCards.push(null);
    }

    const renderCircles = () => {
      // Adding two extra circles Add Song and Jam
      return songList.concat(dummyCards).map((_, idx) => {
        return (
          <div
            key={idx}
            className={classes.circle}
            style={{
              backgroundColor: currentCardFocus === idx
                ? theme.chordPalette.chordLine
                : 'grey'
            }}
            onClick={this.onCirclePress(idx)}>
          </div>
        );
      });
    }

    return (
      <div className={classes.circleContainer}>
        {renderCircles()}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SongScrubber);
