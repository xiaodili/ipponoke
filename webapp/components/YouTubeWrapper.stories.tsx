import * as React from 'react';
import YouTubeWrapper from './YouTubeWrapper';
import { YouTubeWrapper as YouTubeWrapperElement } from './YouTubeWrapper';

import {
  makeStyles
} from '@material-ui/core/styles';

import {
  IconButton,
} from '@material-ui/core';

export default {
  title: 'YouTubeWrapper'
};

const useStyles = makeStyles({
  sectionTitle: {
    /* cursor: 'pointer', */
  }
});

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import RepeatIcon from '@material-ui/icons/Repeat';

export const withText = () => {
  /* https://github.com/mui-org/material-ui/issues/14018 */
  const classes = useStyles({});

  /* const stupidThingLink: string = 'https://www.youtube.com/watch?v=sTm7aZE6u6w'; */
  const stupidThingId: string = 'sTm7aZE6u6w';

  const bookmarks: [string, number][] = [
    ['Intro', 5],
    ['Verse 1', 15],
    ['Chorus 1', 42],
    ['Verse 2', 65],
    ['Chorus 2', 92],
    ['Solo', 114],
    ['Verse 3', 143],
    ['Chorus 3', 172],
    ['Outro', 197],
  ];

  const youTubeWrapper: React.RefObject<YouTubeWrapperElement> = React.createRef();

  const playSection = (seconds: number) => {
    if (youTubeWrapper.current) {
      youTubeWrapper.current.scrubAndPlay(seconds);
    }
  };

  const playSectionLoop = (idx: number) => {

    const start = bookmarks[idx][1];

    if (youTubeWrapper.current) {

      if (bookmarks.length - 1 === idx) {

        youTubeWrapper.current.scrubAndLoop(start);

      } else {

        const end = bookmarks[idx + 1][1];

        youTubeWrapper.current.scrubAndLoop(start, end);
      }
    }
  };

  const play = () => {
    youTubeWrapper.current.play();
  };

  const pause = () => {
    youTubeWrapper.current.pause();
  };

  const getState = () => {
    console.log(youTubeWrapper.current.player.getPlayerState());
  }

  const changeSpeed = (speed: number) => {

    youTubeWrapper.current.changeSpeed(speed);
  };

  return (
    <div>
      <ol>
        {bookmarks.map((bookmark, idx) => {
          const [name, seconds] = bookmark;
          return (
            <li
              key={idx}
              className={classes.sectionTitle} >
              {name}
              <IconButton
                onClick={() => playSection(seconds)} >
                <PlayArrowIcon />
              </IconButton>
              <IconButton onClick={() => playSectionLoop(idx)}>
                <RepeatIcon />
              </IconButton>
            </li>
          );
        })}
      </ol>
      <button onClick={play}>Play</button>
      <button onClick={pause}>Pause</button>
      <button onClick={getState}>Get State</button>
      <button onClick={() => changeSpeed(0.25)}>25%</button>
      <button onClick={() => changeSpeed(0.5)}>50%</button>
      <button onClick={() => changeSpeed(0.75)}>75%</button>
      <button onClick={() => changeSpeed(1)}>100%</button>
      <YouTubeWrapper
        ref={youTubeWrapper}
        videoId={stupidThingId} />
    </div>
  );
}
