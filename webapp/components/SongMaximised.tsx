import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';

import {
  createStyles,
  withStyles,
  IconButton,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import ScaleSelector from './ScaleSelector';
import SongSections from './SongSections';
import PlaybackRateSelector from './PlaybackRateSelector';

import MinimizeIcon from '@material-ui/icons/Minimize';
import CloseIcon from '@material-ui/icons/Close';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import RepeatIcon from '@material-ui/icons/Repeat';
import PauseIcon from '@material-ui/icons/Pause';

const styles = (theme: Theme) => createStyles({
  container: {
    position: 'fixed',
    backgroundColor: 'white',
    width: '100%',
    top: '0px',
    left: '0px',
    height: '100%',
    boxSizing: 'border-box',
    zIndex: 1,
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  menuContainer: {
    display: 'flex',
    position: 'fixed',
    top: '0px',
    right: '0px',
  },
  optionsContainer: {
    display: 'flex',
    alignItems: 'space-end',
    justifyContent: 'space-between',
  },
  titleContainer: {
    marginBottom: theme.spacing(1.5)
  },
  preformattedContainer: {
    fontFamily: 'monospace',
    columns: '300px auto',
    height: '100%',
    overflowX: 'auto',
    whiteSpace: 'pre-wrap',
    columnFill: 'auto',
    columnRule: 'solid 1px grey',
  },
});

interface Props extends WithStyles<typeof styles> {
  song: any;
  removeSong: (id: string) => void;
  minimiseSong: () => void;
  currentScale: string;
  originalKey: string;
  fontSize: number;
  onPlaybackChangePressed: (speed: number) => void;
  pause: () => void;
  playSection: (seconds: number) => void;
  playSectionLoop: (start: number, end?: number) => void;
}


export class SongMaximised extends React.Component<Props, {
  originalKey: string,
  currentScale: string
}> {

  constructor(props: Props) {
    super(props);

    this.state = {
      originalKey: props.originalKey,
      currentScale: props.currentScale,
    };
  }

  private onScaleChanged = (newScale: string) => {

    this.setState({
      currentScale: newScale,
    });
  }

  render() {

    const {
      classes,
      removeSong,
      minimiseSong,
      song,
      fontSize,
      currentScale,
      pause,
      playSection,
      playSectionLoop,
      onPlaybackChangePressed,
    } = this.props;

    const {
      originalKey,
    } = this.state;

    const renderSongMetadata = (metadata: any) => {

      const {
        artist,
      } = metadata;

      return (
        <div className={classes.titleContainer}>
          {artist &&
            <Typography
              variant='caption' >
              {`by ${artist}`}
            </Typography>}
        </div>
      );
    }

    const containsYouTubeLink = song.metadata && song.metadata.link;

    return (
      <div className={classes.container}>
        <div className={classes.menuContainer}>
          {containsYouTubeLink && <PlaybackRateSelector
            onQuarterPressed={() => onPlaybackChangePressed(0.25)}
            onHalfPressed={() => onPlaybackChangePressed(0.5)}
            onThreeQuartersPressed={() => onPlaybackChangePressed(0.75)}
            onFullPressed={() => onPlaybackChangePressed(1)}
          />}
          <IconButton
            size="small"
            onClick={minimiseSong}>
            <MinimizeIcon />
          </IconButton>
          <IconButton
            size="small"
            onClick={() => removeSong(song.id)}>
            <CloseIcon />
          </IconButton>
        </div>
        <div className={classes.preformattedContainer} style={{
          fontSize: `${fontSize}px`
        }}>
          <Typography variant='h6'>
            {song.title}
            <IconButton
              size="small"
              onClick={() => playSection(0)}>
              <PlayCircleFilledIcon />
            </IconButton>
            <IconButton
              size="small"
              onClick={() => playSectionLoop(0)}>
              <RepeatIcon />
            </IconButton>
            <IconButton
              size="small"
              onClick={pause}>
              <PauseIcon />
            </IconButton>
          </Typography>
          {renderSongMetadata(song.metadata)}
          <ScaleSelector
            scale={currentScale}
            originalKey={originalKey}
            onScaleChanged={this.onScaleChanged}
          />
          <SongSections
            sections={song.sections}
            currentScale={currentScale}
            originalKey={originalKey}
            pause={this.props.pause}
            playSection={this.props.playSection}
            playSectionLoop={this.props.playSectionLoop}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(observer(SongMaximised));
