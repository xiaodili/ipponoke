import * as React from 'react';
import {
  createStyles,
  Dialog,
  IconButton,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';

import CloseIcon from '@material-ui/icons/Close';

const titleStyles = (theme: Theme) => createStyles({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    /* display: 'flex',
     * alignItems: 'center',
     * justifyContent: 'space-between', */
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

interface TitleProps extends WithStyles<typeof titleStyles> {
  onClose: () => void;
  children: React.ReactNode;
}

const DialogTitle = withStyles(titleStyles)((props: TitleProps) => {

  const {
    children,
    classes,
    onClose
  } = props;

  return (
    <MuiDialogTitle
      disableTypography
      className={classes.root}>
      <Typography variant='h6'>{children}</Typography>
      <IconButton
        size="small"
        className={classes.closeButton}
        onClick={onClose}>
        <CloseIcon />
      </IconButton>
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

interface Binding {
  text: string;
  binding: string;
};

const keyBindings: Binding[] = [{
  text: 'Toggle start/stop scroll',
  binding: 'p',
}, {
  text: 'Faster scroll speed',
  binding: 'f',
}, {
  text: 'Slower scroll speed',
  binding: 's',
}, {
  text: 'Go to start of song',
  binding: 'gg',
}, {
  text: 'Go to end of song',
  binding: 'G',
}, {
  text: 'Open YouTube link',
  binding: 'o',
}, {
  text: 'Previous song/card',
  binding: 'h',
}, {
  text: 'Next song/card',
  binding: 'l',
}, {
  text: 'Toggle maximise/minimise song',
  binding: 'm',
}, {
  text: 'Search for song',
  binding: '/',
}, {
  text: 'Toggle open/close help dialog',
  binding: '?',
}];


const styles = (theme: Theme) => createStyles({
  bindingText: {
    fontFamily: 'monospace',
  },
  bindingRow: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bindingsContainer: {
    minWidth: '300px',
    '& > div:nth-child(odd)': {
      backgroundColor: theme.cardPalette.dark,
    }
  }
});

interface Props extends WithStyles<typeof styles> {
  onRef: (hd: HelpDialog) => void;
}

export class HelpDialog extends React.Component<Props, {
  open: boolean;
}> {

  constructor(props: Props) {
    super(props);

    this.handleClose = this.handleClose.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.toggleDialog = this.toggleDialog.bind(this);

    this.state = {
      open: false,
    }
  }

  private openDialog() {
    this.setState({
      open: true,
    });
  }

  private handleClose() {
    this.setState({
      open: false,
    });
  }

  public toggleDialog() {
    if (this.state.open) {
      this.handleClose();
    } else {
      this.openDialog();
    }
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  componentWillUnmount() {
    this.props.onRef(null);
  }

  render() {

    const {
      classes,
    } = this.props;

    return (
      <Dialog
        onClose={this.handleClose}
        open={this.state.open} >
        <DialogTitle
          onClose={this.handleClose}>
          Keyboard Shortcuts
        </DialogTitle>
        <DialogContent>
          <div className={classes.bindingsContainer}>
            {keyBindings.map((b: Binding, idx: number) => {
              return (
                <div
                  className={classes.bindingRow}
                  key={idx} >
                  <Typography>{b.text}</Typography>
                  <div className={classes.bindingText}>{b.binding}</div>
                </div>
              );
            })}
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(styles)(HelpDialog);
