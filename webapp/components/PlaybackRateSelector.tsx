import * as React from 'react';
import {
  createStyles,
  FormHelperText,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

const styles = (theme: Theme) => createStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  playbackContainer: {
    margin: theme.spacing(1),
    '& > span': {
      margin: theme.spacing(0.5),
      padding: theme.spacing(0.5),
      cursor: 'pointer',
      borderRadius: '5px',
      backgroundColor: '#cccccc',
    }
  }
});

interface Props extends WithStyles<typeof styles> {
  onQuarterPressed: () => void;
  onHalfPressed: () => void;
  onThreeQuartersPressed: () => void;
  onFullPressed: () => void;
}

class PlaybackRateSelector extends React.Component<Props> {

  constructor(props: Props) {
    super(props);

  }

  render() {

    const {
      classes,
      onQuarterPressed,
      onHalfPressed,
      onThreeQuartersPressed,
      onFullPressed,
    } = this.props;

    return (
      <div className={classes.container}>
        <FormHelperText>Playback Rate</FormHelperText>
        <div className={classes.playbackContainer}>
          <Typography variant='caption' onClick={onQuarterPressed}>
            25%
          </Typography>
          <Typography variant='caption' onClick={onHalfPressed}>
            50%
          </Typography>
          <Typography variant='caption' onClick={onThreeQuartersPressed}>
            75%
          </Typography>
          <Typography variant='caption' onClick={onFullPressed}>
            100%
          </Typography>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(PlaybackRateSelector);
