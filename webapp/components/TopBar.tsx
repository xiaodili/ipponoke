import * as React from 'react';
import {
  withStyles,
} from '@material-ui/core/styles';

import {
  createStyles,
  AppBar,
  Theme,
  Toolbar,
  Typography,
  WithStyles
} from '@material-ui/core';

import SongSelect from './SongSelect';
import KeyProcessor from '../lib/KeyProcessor';

const styles = (theme: Theme) => createStyles({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
  },
});

interface Props extends WithStyles<typeof styles> {
  addSong: (songId: string) => void,
  songList: any[];
  metadatas: any[];
  keyProcessor: KeyProcessor;
  theme: Theme,
}

class TopBar extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
  }

  render() {

    const {
      addSong,
      classes,
      metadatas,
      keyProcessor,
      songList,
      theme,
    } = this.props;

    return (
      <div className={classes.root}>
        <AppBar
          position="static"
          color="secondary" >
          <Toolbar className={classes.toolbar}>
            <div>
              <Typography variant="h6" color="inherit">
                Ipponoke
              </Typography>
            </div>
            <SongSelect
              keyProcessor={keyProcessor}
              addSong={addSong}
              metadatas={metadatas}
              selectedSongIds={songList.map(s => s.id)}
            />
          </Toolbar>
        </AppBar>
      </div>
    );
  }

}

export default withStyles(styles, { withTheme: true })(TopBar);
