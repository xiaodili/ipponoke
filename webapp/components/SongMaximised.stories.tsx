import * as React from 'react';
import SongMaximised from './SongMaximised';

export default {
  title: 'SongMaximised'
};

export const withNormalSong = () => {

  const noOp: () => null = () => null;

  const songData = {
    "id": "moon-river",
    "title": "Moon River",
    "metadata": {
      "artist": "Audrey Hepburn",
      "key": "F",
      "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
    },
    "sections": [{
      "name": "Verse 1",
      "metadata": {},
      "lines": [
        "F    Dm     A#           F          ",
        "Moon River, wider than a mile,  I'm",
        "A#              F            Gm/E  A7     ",
        "crossing you in style some   day.      Old",
        "Dm    F7         A#   D#9-5          ",
        "dream-maker, you heartbreaker, Where-",
        "Dm   Dm7    Dm7/B E7      ",
        "ever you're goin’,     I’m",
        "Am7   D7      Gm7   C9  ",
        "goin’    your way."
      ]
    }, {
      "name": "Verse 2",
      "metadata": {},
      "lines": [
        "F    Dm        A#             F              ",
        "Two  drifters, off to see the world; there's",
        "A#            F        Gm/E   A7       ",
        "such a lot of world to see.      We're",
        "Dm   Dm/C    Dm/B   A#        Am7   ",
        "af - ter the same    rainbow's end, ",
        "A#                   F       ",
        "  waiting ’round the bend — ",
        "A#               F       ",
        "  my huckleberry friend,",
        "Dm    Gm      C7         F",
        "Moon River,     and     me."
      ]
    }]
  };

  return (
    <div>
      <SongMaximised
        song={songData}
        removeSong={noOp}
        minimiseSong={noOp}
        currentScale={"F"}
        originalKey={"F"}
        fontSize={13}
        onPlaybackChangePressed={noOp}
        pause={noOp}
        playSection={noOp}
        playSectionLoop={noOp} />
    </div >
  );
}
