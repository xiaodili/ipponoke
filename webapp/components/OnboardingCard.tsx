import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import { version } from '../package.json';

import {
  Card,
  CardContent,
  createStyles,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

const styles = (theme: Theme) => createStyles({
  version: {
    display: 'flex',
    justifyContent: 'center',
  },
  contentContainer: {
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  songContainer: {
    overflow: 'auto',
    height: '100%',
    width: '100%',
    boxSizing: 'border-box',
    display: 'flex',
  },
  icon: {
    cursor: 'pointer',
  },
  headerContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  welcomeBody: {
    background: theme.palette.background.paper,
    padding: theme.spacing(1),
  }
});

interface Props extends WithStyles<typeof styles> {
  onRemove: () => void;
}

@observer
class OnboardingCard extends React.Component<Props, {
}> {

  constructor(props: Props) {
    super(props);

  }

  render() {
    const {
      classes,
      onRemove,
    } = this.props;

    return (
      <Card className={classes.songContainer}>
        <CardContent>
          <div className={classes.contentContainer}>
            <div>
              <div className={classes.headerContainer}>
                <Typography variant='h6' gutterBottom>Bon Jovi!</Typography>
                <CloseIcon onClick={onRemove} className={classes.icon} />
              </div>
              <div className={classes.welcomeBody}>

                <Typography gutterBottom>
                  Use Ipponoke for quickly loading up songs to play along/sing with.
                </Typography>

                <Typography gutterBottom>
                  If playing with others, keep songs in sync by joining a Jam Room.
                </Typography>

                <Typography gutterBottom>
                  Press <code>?</code> to see what keybindings are available.
                </Typography>
              </div>
            </div>
            <Typography
              variant='caption'
              className={classes.version}>
              v{version}
            </Typography>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(OnboardingCard);
