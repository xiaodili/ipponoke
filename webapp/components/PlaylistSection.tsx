import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import {
  Button,
  Collapse,
  createStyles,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  withStyles,
  WithStyles,
  Theme,
  Typography,
} from '@material-ui/core';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const styles = (theme: Theme) => createStyles({
  nested: {
    paddingLeft: theme.spacing(2),
  },
  joinButton: {
    marginRight: theme.spacing(1),
  },
});

@observer
class PlaylistSection extends React.Component<Props, { expanded: boolean }> {

  constructor(props: Props) {

    super(props);

    this.state = {
      expanded: false
    }
  }

  toggleExpand = () => {
    this.setState({
      expanded: !this.state.expanded,
    })
  }

  render() {

    const {
      classes,
      songs,
      name,
      onAddPlaylist,
      addSong,
    } = this.props;

    const {
      expanded,
    } = this.state;

    const renderSong = (song: {
      id: string,
      artist: string,
      title: string
    }) => {
      return (
        <ListItem
          divider
          dense
          key={song.id}>
          <ListItemText
            primary={`${song.artist} - ${song.title}`}
          />
          <ListItemSecondaryAction>
            <Button onClick={() => addSong(song.id)}>
              Add
            </Button>
          </ListItemSecondaryAction>
        </ListItem>
      );
    };

    return (
      <div>
        <ListItem
          divider
          button
          onClick={this.toggleExpand}
          dense >
          <ListItemText
            disableTypography={true}
            primary={<Typography variant='subtitle2'>{name}</Typography>}
            secondary={<div>
              <Typography variant='caption'>
                Number of songs: {songs.length}
              </Typography>
            </div>
            }
          />
          <ListItemSecondaryAction>
            <Button
              color='primary'
              variant='contained'
              size='small'
              onClick={onAddPlaylist}
              className={classes.joinButton} >
              Add
          </Button >
            {expanded ? <ExpandLess /> : <ExpandMore />}
          </ListItemSecondaryAction>
        </ListItem>
        <Collapse in={expanded}>
          <List
            dense
            className={classes.nested}
            disablePadding>
            {songs.map(renderSong)}
          </List>
        </Collapse>
      </div>
    );
  }
}

interface Props extends WithStyles<typeof styles> {
  songs: any[];
  name: string;
  onAddPlaylist: () => void;
  addSong: (id: string) => void;
}

export default withStyles(styles)(PlaylistSection);
