import {
  lineContainsOnlyChords,
} from '../../ipponokese/notation';

export const joinClasses = (...classes: string[]) => {
  return classes.join(' ');
};

import * as _ from 'lodash';

export const isWhitespace = (char: string): boolean => {
  return char === ' ' || char === '　';
}

export const parseDuration = (durationStr: string): number => {

  const fragments = durationStr.split(':');

  if (fragments.length === 1) { // youtube-time: "300"
    return parseInt(fragments[0]);
  } else if (fragments.length === 2) { // youtube-time: "3:15"
    return parseInt(fragments[0]) * 60 + parseInt(fragments[1]);
  } else {
    throw new Error(`not a valid time: ${durationStr}`);
  }
};

export const transposeGroupedLine = (groupedLine: any) => {

  console.assert(groupedLine.chord || groupedLine.lyric);

  const hasChordOnly = 'chord' in groupedLine && !('lyric' in groupedLine);
  const hasLyricOnly = 'lyric' in groupedLine && !('chord' in groupedLine);

  if (hasChordOnly) {

    return Array.from(groupedLine.chord).map(c => {
      return {
        chord: c
      };
    });

  } else if (hasLyricOnly) {

    return Array.from(groupedLine.lyric).map(c => {
      return {
        lyric: c
      };
    })

  } else { // has both

    const padLength = Math.max(groupedLine.chord.length, groupedLine.lyric.length);

    return _.zip(
      Array.from(groupedLine.chord.padEnd(padLength, '\xa0')),
      Array.from(groupedLine.lyric.padEnd(padLength, '\xa0'))).map(cl => {
        return {
          chord: cl[0],
          lyric: cl[1]
        };
      });
  }
};

export const groupChordLyricLines = (lines: string[], transformChord: (s: string) => string): any[] => {

  const initialGroupings = [{}];

  return lines.reduce((groupings: any[], line: string) => {

    const currentGrouping = groupings[groupings.length - 1];
    const isChordLine = lineContainsOnlyChords(line);

    if (isChordLine) {
      if ('chord' in currentGrouping || 'lyric' in currentGrouping) {
        const newCell = {
          chord: transformChord(line),
        };
        return groupings.concat(newCell);
      } else {
        currentGrouping.chord = transformChord(line);
        return groupings;
      }
    } else { // contains lyrics
      if ('lyric' in currentGrouping) {
        const newCell = {
          lyric: line,
        };
        return groupings.concat(newCell);
      } else {
        currentGrouping.lyric = line;
        return groupings;
      }
    }
  }, initialGroupings);
}
