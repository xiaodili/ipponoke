import createTheme from '@material-ui/core/styles/createTheme';
import deepOrange from '@material-ui/core/colors/deepOrange';

// https://material-ui.com/guides/typescript/
declare module '@material-ui/core/styles/createTheme' {
  interface Theme {
    chordPalette: {
      chordLine: string;
    }
    cardPalette?: {
      light?: string;
      dark?: string;
    }
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    chordPalette?: {
      chordLine?: string;
    }
    cardPalette?: {
      light?: string;
      dark?: string;
    }
  }
}


const theme = createTheme({
  transitions: {
    create: () => 'none',
  },
  palette: {
    primary: {
      main: deepOrange[200]
    },
    secondary: {
      main: deepOrange[400]
    },
  },
  cardPalette: {
    light: '#F5F5F5',
    dark: '#E0E0E0',
  },
  chordPalette: {
    /* chordLine: '#FF530D' */
    chordLine: deepOrange[500]
  },
  /* https://material-ui.com/getting-started/faq/#how-can-i-disable-the-ripple-effect-globally */
  props: {
    MuiButtonBase: {
      disableRipple: true,
    }
  },
});

export default theme;
