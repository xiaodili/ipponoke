import SongStore from '../stores/SongStore';
import { Root } from '../containers/Root';
import { HelpDialog } from '../components/HelpDialog';
import { SongSelect } from '../components/SongSelect';
import { Song } from '../components/Song';

class KeyProcessor {

  private songStore: SongStore;
  private songSelect: SongSelect;
  private rootElement: Root;
  private helpDialog: HelpDialog;
  private waitingKeys: string[] = [];

  constructor(songStore: SongStore, rootElement: Root) {

    this.songStore = songStore;
    this.rootElement = rootElement;
    /* https://medium.com/@uistephen/keyboardevent-key-for-cross-browser-key-press-check-61dbad0a067a */
    document.addEventListener(
      // This is keyup instead of a keydown to stop a '/' being sent to SongSelect when focusing on it
      'keyup',
      (event: KeyboardEvent) => {
        // This should really be an Element, but TS is suggesting Ref<any>. Casting it to unknown
        const inputRef: unknown = this.songSelect.selectRef.current.select.inputRef;
        const songSelectIsActive = (window.document.activeElement === inputRef);

        if (!songSelectIsActive) {
          this.handleKeyPress(event.key || event.keyCode.toString());
        }
      }
    );
  }

  public setHelpDialog(helpDialog: HelpDialog) {
    this.helpDialog = helpDialog;
  }

  public setSongSelect(songSelect: SongSelect) {
    this.songSelect = songSelect;
  }

  private getFocusedSong(): Song {

    return this.rootElement.songsElement.songElements[
      this.songStore.songList[
        this.songStore.currentCardFocus].id];
  }

  private togglePlayPause() {

    if (this.songStore.focusedOnSong) {

      //only functional if pressed while maximised
      if (!this.getFocusedSong().state.maximised) {
        this.getFocusedSong().toggleAutoScroll();
      }
    }
  }

  private faster() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().fastScroll();
    }
  }

  private slower() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().slowScroll();
    }
  }

  private goToBottom() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().goToBottom();
    }
  }

  private goToTop() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().goToTop();
    }
  }

  private openYouTube() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().openYouTube();
    }
  }

  private toggleMaximiseMinimise() {
    if (this.songStore.focusedOnSong) {
      this.getFocusedSong().toggleMaximise();
    }
  }

  private focusOnSongSelect() {
    this.songSelect.selectRef.current.focus();
  }

  private toggleDialog() {
    this.helpDialog.toggleDialog();
  }

  private focusLeft() {

    const newIdx = Math.max(0, this.songStore.currentCardFocus - 1);

    if (newIdx !== this.songStore.currentCardFocus) {

      if (this.songStore.focusedOnSong && this.getFocusedSong().state.maximised) {

        this.getFocusedSong().minimiseSong();
        this.changeCardFocus(newIdx);
        this.getFocusedSong().maximiseSong();

      } else {
        this.changeCardFocus(newIdx);
      }
    }
  }

  private changeCardFocus(newIdx: number) {

    // TODO: Address fn duplication with the onCirclePress function in SongScrubber
    const se = this.rootElement.songsElement;
    this.songStore.changeCardFocus(newIdx);
    se.onCirclePressed(newIdx);
  }

  private focusRight() {

    const se = this.rootElement.songsElement;

    const numCards = this.songStore.songList.length
      + (se.state.onboardingDismissed
        ? 0
        : 1)
      + 2; // songsCard, jamCard

    const newIdx = Math.min(numCards - 1, this.songStore.currentCardFocus + 1);

    if (newIdx !== this.songStore.currentCardFocus) {

      if (this.songStore.focusedOnSong && this.getFocusedSong().state.maximised) {

        if (newIdx < this.songStore.songList.length) {
          this.getFocusedSong().minimiseSong();
          this.changeCardFocus(newIdx);
          this.getFocusedSong().maximiseSong();
        }

      } else {
        this.changeCardFocus(newIdx);
      }
    }
  }

  private handleKeyPress(key: string) {
    this.waitingKeys.push(key);

    if (this.waitingKeys[0] === 'g' && this.waitingKeys.length === 1) {
      return;

    } else if (this.waitingKeys[0] === 'p' && this.waitingKeys.length === 1) {

      this.togglePlayPause();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'f' && this.waitingKeys.length === 1) {

      this.faster();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 's' && this.waitingKeys.length === 1) {

      this.slower();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === '?' && this.waitingKeys.length === 1) {

      this.toggleDialog();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === '/' && this.waitingKeys.length === 1) {

      this.focusOnSongSelect();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'o' && this.waitingKeys.length === 1) {

      this.openYouTube();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'G' && this.waitingKeys.length === 1) {

      this.goToBottom();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'm' && this.waitingKeys.length === 1) {

      this.toggleMaximiseMinimise();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'h' && this.waitingKeys.length === 1) {

      this.focusLeft();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'l' && this.waitingKeys.length === 1) {

      this.focusRight();
      this.waitingKeys = [];

    } else if (this.waitingKeys[0] === 'g' && this.waitingKeys.length === 2) {

      this.goToTop();
      this.waitingKeys = [];

    } else { // Invalid command

      this.waitingKeys = [];
    }
  }
}

export default KeyProcessor;
