import axios from 'axios';
import * as _ from 'lodash';
import * as io from 'socket.io-client';
import NotificationStore from './NotificationStore';

import {
  action,
  computed,
  observable,
} from 'mobx';

const apiClient = axios.create({
  baseURL: '/api',
})

class SongStore {

  // TS checker not recognising the decorator

  // Loaded songs
  public songList = observable([] as any[]);
  // Catalogue
  public metadatas = observable([] as any[]);
  // Jam Rooms
  public jams = observable([] as any[]);

  @observable public loadingMetadatas: boolean = true;

  @observable public jamId?: string;

  @observable public id?: string;

  @observable public playlists = observable([] as any[]);

  @observable public joiningJamId?: string;
  @observable public addingSongId?: string;

  // UI for showing which circle is focued
  @observable public currentCardFocus: number = 0;

  private socket: SocketIOClient.Emitter;
  private serverSession: string;
  private notificationStore: NotificationStore;

  private initiateSockets() {

    const socket = io();

    socket.on('connect', () => {

      socket.emit('SYNC', {
        id: this.id,
      });
    });

    socket.on('SYNCED', action(({ id, jams, serverSession }: { id: string, jams: any[], serverSession: string }) => {

      this.id = id;
      this.jams.replace(jams);

      if (this.serverSession === serverSession) {
        if (this.id && this.jamId) { //try to rejoin if you had been temporarily disconnected

          if (jams.findIndex(j => j.id === this.jamId) > -1) {

            this.joinJam(this.jamId);

          } else {
            this.jamId = null;
          }
        }
      } else { //Server's restarted since then; resync metadatas
        // Hmm... how to deal with jam sessions here though?
      }
    }));

    socket.on('SONG_REMOVED', ({ songId, jamId }: { songId: string, jamId: string }) => {

      const jamIdx = this.jams.findIndex(j => j.id === jamId);
      const jam = this.jams[jamIdx];
      const songIdx = jam.songs.indexOf(songId);
      if (songIdx > -1) {
        jam.songs.splice(songIdx, 1);
      }

      if (this.jamId && this.jamId === jamId) {

        const songIdx = this.songList.findIndex((s) => {
          return s.id === songId;
        });

        if (songIdx > -1) {
          this.songList.splice(songIdx, 1);
        }
      }
    });

    socket.on('SONG_ADDED', ({ songId, jamId }: { songId: string, jamId: string }) => {

      const jamIdx = this.jams.findIndex(j => j.id === jamId);
      const jam = this.jams[jamIdx];
      const songIdx = jam.songs.indexOf(songId);
      if (songIdx === -1) {
        jam.songs.push(songId);
      }

      if (this.jamId && this.jamId === jamId) {

        const songIdx = this.songList.findIndex((s) => {
          return s.id === songId;
        });

        if (songIdx === -1 && songId !== this.addingSongId) {
          this.addSong(songId);
        }

      }

    });

    socket.on('JAM_JOINED', action(({ indId, jamId }: { indId: string, jamId: string }) => {

      const jamIdx = this.jams.findIndex(j => j.id === jamId);
      const jam = this.jams[jamIdx];
      const indIdx = jam.participants.indexOf(indId);

      if (indIdx === -1) {
        jam.participants.push(indId);
      }
    }));

    socket.on('JAM_LEFT', ({ indId, jamId }: { indId: string, jamId: string }) => {

      const jamIdx = this.jams.findIndex(j => j.id === jamId);
      const jam = this.jams[jamIdx];
      const indIdx = jam.participants.indexOf(indId);
      if (indIdx > -1) {
        jam.participants.splice(indIdx, 1);
      }
    });

    // Same as above
    socket.on('INDIVIDUAL_DISCONNECTED', ({ indId, jamId }: { indId: string, jamId: string }) => {
      const jamIdx = this.jams.findIndex(j => j.id === jamId);
      const jam = this.jams[jamIdx];
      const indIdx = jam.participants.indexOf(indId);
      if (indIdx > -1) {
        jam.participants.splice(indIdx, 1);
      }
    });

    this.socket = socket;
  }

  constructor(notificationStore: NotificationStore) {

    this.notificationStore = notificationStore;

    this.addRandomSong = this.addRandomSong.bind(this);
    this.addRandomSongs = this.addRandomSongs.bind(this);
    this.addSong = this.addSong.bind(this);
    this.addSongs = this.addSongs.bind(this);
    this.addSongAt = this.addSongAt.bind(this);
    this.changeCardFocus = this.changeCardFocus.bind(this);
    this.getSongIdx = this.getSongIdx.bind(this);
    this.joinJam = this.joinJam.bind(this);
    this.addPlaylist = this.addPlaylist.bind(this);
    this.leaveJam = this.leaveJam.bind(this);
    this.removeSong = this.removeSong.bind(this);
    this.initiateSockets = this.initiateSockets.bind(this);

    /* this.getEverything(); */
    this.getMetadatas().then(this.initiateSockets);

  }

  @computed
  public get focusedOnSong() {
    return this.songList.length - 1 >= this.currentCardFocus;
  }

  @action
  public changeCardFocus(idx: number) {
    this.currentCardFocus = idx;
  }

  public getSongIdx(songId: string): number {

    const idx = this.songList.findIndex((song) => {
      return song.id === songId;
    });
    return idx;
  }

  @action
  private getMetadatas() {

    this.loadingMetadatas = true;

    return apiClient.get('/metadatas').then((payload) => {

      const {
        metadatas,
        serverSession,
        playlists,
      } = payload.data;

      this.serverSession = serverSession;

      // https://mobx.js.org/refguide/array.html
      this.metadatas.replace(metadatas);
      this.playlists.replace(playlists);
      this.loadingMetadatas = false;
    })
  }

  @action
  public addSongAt(songId: string) {

    this.addingSongId = songId;

    apiClient.post(`/song/${songId}`, {
      jamId: this.jamId,
    }).then((payload) => {

      const song = payload.data.song;
      this.songList.splice(this.currentCardFocus, 0, song);
      this.addingSongId = null;
    });
  }

  @action
  public addRandomSong() {

    const songId = _.sample(this.metadatas).id;

    apiClient.post(`/song/${songId}`, {
      jamId: this.jamId,
    }).then((payload) => {

      const song = payload.data.song;
      this.songList.push(song);
      this.addingSongId = null;
    });
  }

  @action
  public addRandomSongs(...keys: string[]) {

    const songsByScale = _.toPairs(_.groupBy(this.metadatas, 'key'));

    const songScalesToInclude = songsByScale.filter(([key, songs]) => {
      if (keys.length === 0) {
        return true;
      } else {
        return keys.map(k => k.toLowerCase()).indexOf(key.toLowerCase()) > -1;
      }
    }).map(([key, songs]) => {
      return songs;
    });

    const randomSongIds = songScalesToInclude.map((songs) => {
      return _.sample(songs).id;
    })

    const randomSongIdsNotAdded = randomSongIds.filter((songId) => {
      return this.songList.map(song => song.id).indexOf(songId) === -1;
    })

    apiClient.post(`/songs`, {
      songIds: randomSongIdsNotAdded,
      jamId: this.jamId,
    }).then((payload) => {

      const songs = payload.data.songs;
      this.songList.push(...songs);
    });
  }

  @action
  public addSong(songId: string) {

    this.addingSongId = songId;

    return apiClient.post(`/song/${songId}`, {
      jamId: this.jamId,
    }).then((payload) => {

      const song = payload.data.song;
      this.songList.push(song);
      this.addingSongId = null;
      this.notificationStore.setMessage(`Added ${song.title}`)
    });
  }

  @action
  public addSongs(songIds: string[]) {

    apiClient.post(`/songs`, {
      songIds,
      jamId: this.jamId,
    }).then((payload) => {

      const songs = payload.data.songs;
      this.songList.push(...songs);
    });
  }

  @action
  public createJam() {

  }

  @action
  public joinJam(jamId: string) {

    this.joiningJamId = jamId;

    apiClient.post('/jams/join', {

      jamId,
      indId: this.id

    }).then((payload) => {

      this.joiningJamId = null;
      this.jamId = jamId;

      const songs = payload.data.songs;

      this.songList.replace(songs);
    });
  }

  @action
  public addPlaylist(playlistId: string) {

    return apiClient.get(`/playlists/${playlistId}`).then((payload) => {

      const songs = payload.data.songs;

      this.songList.replace(songs);
    });
  }

  // deprecated?
  @action
  private getEverything() {
    apiClient.get('/everything').then((payload) => {

      // https://mobx.js.org/refguide/array.html
      this.songList.replace(payload.data.songs);
      /* this.songList.replace(payload.data.songs.slice(0, 2)); */
    })
  }

  @action
  public leaveJam() {

    const jamIdToLeave = this.jamId;

    this.jamId = null;

    apiClient.post('/jams/leave', {
      indId: this.id,
      jamId: jamIdToLeave,
    });
  }


  @action
  public removeSong(songId: string) {

    const songIdx = this.songList.findIndex((s) => {
      return s.id === songId;
    });

    this.songList.splice(songIdx, 1);

    this.socket.emit('REMOVE_SONG', {
      songId,
      jamId: this.jamId,
    });
  }
}

export default SongStore;
