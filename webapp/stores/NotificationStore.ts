import { action, observable } from 'mobx';
class NotificationStore {

  @observable public message?: string;

  @action
  public setMessage = (message: string): void => {
    this.message = message;
  }

  @action
  public dismissMessage = (): void => {
    this.message = null;
  }
}

export default NotificationStore;
