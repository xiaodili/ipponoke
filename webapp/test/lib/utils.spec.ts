import { expect } from 'chai';
import {
  describe,
  it,
} from 'mocha';

import {
  parseDuration,
  groupChordLyricLines,
  transposeGroupedLine,
} from '../../lib/utils';

describe('groupChordLyricLines', () => {

  const noOp = (a: string) => a;


  it('should handle paired cases', () => {

    const lines = [
      'Bm                        F#',
      '  On a dark desert highway, cool wind in my hair',
      'A                    E',
      'Warm smell of colitas rising up through the air',
    ];

    const expectedGroupings = [{
      chord: 'Bm                        F#',
      lyric: '  On a dark desert highway, cool wind in my hair',
    }, {
      chord: 'A                    E',
      lyric: 'Warm smell of colitas rising up through the air',
    }];

    const actualGroupings = groupChordLyricLines(lines, noOp);
    expect(actualGroupings).to.deep.equal(expectedGroupings);
  });

  it('should handle pure chord cases', () => {

    const lines = [
      'E   B   A   E',
      'E   B   A   B'
    ];

    const expectedGroupings = [{
      chord: 'E   B   A   E',
    }, {
      chord: 'E   B   A   B',
    }];

    const actualGroupings = groupChordLyricLines(lines, noOp);
    expect(actualGroupings).to.deep.equal(expectedGroupings);
  });

  it('should start a new line when a chord line follows an unchorded lyric line', () => {

    const lines = [
      '(Elder Cunningham)',
      'C              F             C           G',
      '  I\'m about to do it for the first time.',
    ];

    const expectedGroupings = [{
      lyric: '(Elder Cunningham)',
    }, {
      chord: 'C              F             C           G',
      lyric: '  I\'m about to do it for the first time.',
    }];

    const actualGroupings = groupChordLyricLines(lines, noOp);
    expect(actualGroupings).to.deep.equal(expectedGroupings);
  });

  it('should be able to handle chinese characters', () => {

    const lines = [
      'Bbm　 Fm　Gb　Ab　Db',
      '啊　　啊  啊'
    ];

    const expectedGroupings = [{
      chord: 'Bbm　 Fm　Gb　Ab　Db',
      lyric: '啊　　啊  啊'
    }];

    const actualGroupings = groupChordLyricLines(lines, noOp);
    expect(actualGroupings).to.deep.equal(expectedGroupings);
  });
});


describe('transposeGroupedLines', () => {

  it('should transpose chord-only lines', () => {

    const groupedLine = {
      chord: 'Am F C G'
    };

    const expectedTranspose = [{
      chord: 'A'
    }, {
      chord: 'm'
    }, {
      chord: ' '
    }, {
      chord: 'F'
    }, {
      chord: ' '
    }, {
      chord: 'C'
    }, {
      chord: ' '
    }, {
      chord: 'G'
    }];

    const actualTranspose = transposeGroupedLine(groupedLine);
    expect(actualTranspose).to.deep.equal(expectedTranspose);
  });

  it('should transpose lyric-only lines', () => {

    const groupedLine = {
      lyric: 'Oh yeah!',
    };

    const expectedTranspose = [{
      lyric: 'O'
    }, {
      lyric: 'h'
    }, {
      lyric: ' '
    }, {
      lyric: 'y'
    }, {
      lyric: 'e'
    }, {
      lyric: 'a'
    }, {
      lyric: 'h'
    }, {
      lyric: '!'
    }];

    const actualTranspose = transposeGroupedLine(groupedLine);
    expect(actualTranspose).to.deep.equal(expectedTranspose);
  });

  it('should transpose lines containing lyric and chord', () => {

    const groupedLine = {
      chord: 'Am    F ',
      lyric: 'Ha ha ha',
    };

    const expectedTranspose = [{
      chord: 'A',
      lyric: 'H'
    }, {
      chord: 'm',
      lyric: 'a'
    }, {
      chord: ' ',
      lyric: ' '
    }, {
      chord: ' ',
      lyric: 'h'
    }, {
      chord: ' ',
      lyric: 'a'
    }, {
      chord: ' ',
      lyric: ' '
    }, {
      chord: 'F',
      lyric: 'h'
    }, {
      chord: ' ',
      lyric: 'a'
    }];

    const actualTranspose = transposeGroupedLine(groupedLine);

    expect(actualTranspose).to.deep.equal(expectedTranspose);
  });
});

describe('parseDuration', () => {
  it('should parse m:ss times from youtube-time okay', () => {
    const expectedTime = 122;
    const actualTime = parseDuration("2:02");
    expect(actualTime).to.equal(expectedTime);
  });
  it('should parse second times from youtube-time normally', () => {
    const expectedTime = 300;
    const actualTime = parseDuration("300");
    expect(actualTime).to.equal(expectedTime);
  });
});
