const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
  entry: './index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: 'ts-loader',
          // Leave type-checking for the fork plugin
          options: {
            transpileOnly: true,
          }
        },
        exclude: /node_modules/,
      }],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../dist/webapp')
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Ipponoke",
      template: "./index.html"
    }),
    new ForkTsCheckerWebpackPlugin()
  ],
  watchOptions: {
    ignored: /node_modules/,
  }
};
