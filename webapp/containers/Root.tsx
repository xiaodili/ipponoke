import * as React from 'react';
import { observer } from 'mobx-react';

import {
  createStyles,
  CssBaseline,
  Theme,
  withStyles,
  WithStyles
} from '@material-ui/core';

import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';

import theme from '../lib/theme';

import SongStore from '../stores/SongStore';
import NotificationStore from '../stores/NotificationStore';
import KeyProcessor from '../lib/KeyProcessor';

import Songs, { Songs as SongsElement } from '../components/Songs';
import TopBar from '../components/TopBar';
import HelpDialog from '../components/HelpDialog';
import Snackbar from '../components/Snackbar';

const notificationStore = new NotificationStore();
const songStore = new SongStore(notificationStore);

// For debugging
(window as any).songStore = songStore;

const styles = (_: Theme) => createStyles({
  appContainer: {
    // After upgrading to Material-UI v4, line-height got bumped to 1.43. Correcting this.
    lineHeight: 'normal',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '100vh',
  }
});

interface Props extends WithStyles<typeof styles> {
  /* songStore: SongStore, */
}

export class Root extends React.Component<Props> {

  public songsElement: SongsElement;
  private keyProcessor: KeyProcessor;

  public constructor(props: Props) {
    super(props);
    this.keyProcessor = new KeyProcessor(songStore, this);

    // https://stackoverflow.com/questions/1397329/how-to-remove-the-hash-from-window-location-url-with-javascript-without-page-r
    // TODO: We'll need a share button?
    const hashFrag = window.location.hash;
    const noHashURL = window.location.href.replace(/#.*$/, '');
    window.history.replaceState('', document.title, noHashURL);
    try {
      const hashLink = hashFrag.slice(1);
      const paths = hashLink.split("/");
      const collection = paths[0];
      const id = paths[1];
      if (collection === "songs") {
        songStore.addSong(id).catch(e => {
          console.log('e:', e);
        });
      } else if (collection === "playlist") {
        songStore.addPlaylist(id);
      }
    } catch (e) {
      console.log('e:', e);
    }
  }

  /* https://stackoverflow.com/a/51082563/1010076 */
  /* componentDidUpdate(prevProps, prevState) {

   *   if (this.props) {
   *     console.log(`from root.tsx`);
   *     Object.entries(this.props).forEach(([key, val]) =>
   *       prevProps[key] !== val && console.log(`Prop '${key}' changed`)
   *     );
   *   }

   *   if (this.state) {
   *     Object.entries(this.state).forEach(([key, val]) =>
   *       prevState[key] !== val && console.log(`State '${key}' changed`)
   *     );
   *   }
   * } */

  public render() {

    const {
      classes,
    } = this.props;

    // Using onRef function, since wrapping components hides the public functions and refs of the children:
    // https://github.com/kriasoft/react-starter-kit/issues/909
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <div className={classes.appContainer}>

          <Snackbar notificationStore={notificationStore} />

          <HelpDialog onRef={hd => this.keyProcessor.setHelpDialog(hd)} />

          <TopBar
            keyProcessor={this.keyProcessor}
            metadatas={songStore.metadatas}
            addSong={songStore.addSongAt}
            songList={songStore.songList} />

          <Songs
            onRef={r => this.songsElement = r}
            songStore={songStore}
            notificationStore={notificationStore}
          />

        </div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(observer(Root));
