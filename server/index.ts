const _ = require('lodash');
const bodyParser = require('body-parser');
const crypto = require('crypto');
import * as express from 'express';
const fs = require('fs');
const http = require('http');
const io = require('socket.io');
const parser = require('../ipponokese/grammar');
const path = require('path');

const { JamStore } = require('./stores/JamStore');
const { IdStore } = require('./stores/IdStore');
const { PlaylistStore } = require('./stores/PlaylistStore');
const {
  jamNames,
  log,
  IdGenerator,
  containsYoutubeTimings
} = require('./lib/utils');

// This should match up to sites-available/default in deployed location
const PORT = 8021;
// Relative to the dist directory
const DATA_PATH = '../../../ipponokese-data/songs';

const loadSongs = (absDataPath: string) => {

  const idify = new IdGenerator().idify;

  let songs: any[] = [];

  const songFiles = fs.readdirSync(absDataPath);

  log(`Loading song files...`);

  songFiles.forEach((file: String) => {
    const songContents = fs.readFileSync(path.join(absDataPath, file), 'utf8');
    songs = songs.concat(
      parser.parse(songContents).songs.map(idify));
  });

  log(`Loaded ${songFiles.length} song files`);

  return songs;
};

const songsToMetadatas = (songs: any[]) => {
  return songs.map((song: any) => {
    return {
      id: song.id,
      title: song.title,
      ...song.metadata,
      containsTimings: containsYoutubeTimings(song)
    };
  })
};

const start = () => {

  const SERVER_SESSION = crypto.randomBytes(16).toString('hex');

  const app = express();
  /* https://codeforgeek.com/2014/09/handle-get-post-request-express-4/ */
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  const httpServer = http.Server(app);
  const ioServer = io(httpServer);

  const songs = loadSongs(path.join(__dirname, DATA_PATH));
  const metadatas = songsToMetadatas(songs);

  const jamStore = new JamStore();
  const idStore = new IdStore();
  const playlistStore = new PlaylistStore(songs);

  // Create some example Jam rooms
  const numJamRooms = 5;
  const jamRoomNames = _.sampleSize(jamNames, numJamRooms);
  for (let i = 0; i < numJamRooms; i++) {
    jamStore.createJam(jamRoomNames[i]);
  }

  //Socket.io
  //---------
  ioServer.on('connection', (socket: SocketIO.Socket) => {

    socket.on('SYNC', (identity) => {

      let id;

      if (identity.id) {
        id = identity.id;
      } else {
        id = idStore.generate();
      }

      idStore.add(id, socket.id);

      socket.emit('SYNCED', {
        id,
        jams: _.values(jamStore.getAvailableJams()),
      });
    });

    socket.on('REMOVE_SONG', ({ songId, jamId }: { songId: string, jamId?: string }) => {

      if (jamId) {
        jamStore.removeSongFromJam(songId, jamId);
        ioServer.emit('SONG_REMOVED', {
          jamId,
          songId,
        });
      }
    });

    socket.on('disconnect', () => {

      const ind = idStore.remove(socket.id);

      if (ind && ind.jamId) {
        jamStore.removeIndividualFromJam(ind.id, ind.jamId);
        ioServer.emit('INDIVIDUAL_DISCONNECTED', {
          indId: ind.id,
          jamId: ind.jamId,
        });
      }

    });
  });


  //API
  //---
  const api = express();

  api.get('/metadatas', (req, res) => {
    res.set('Content-Type', 'application/json');
    res.send({
      metadatas,
      serverSession: SERVER_SESSION,
      playlists: playlistStore.getPlaylists(),
    });
  });

  // This isn't being used presently
  api.post('/songs', (req, res) => {

    const {
      songIds,
      jamId,
    } = req.body;

    const songIdLookup = new Set(songIds);
    const songsToAdd = songs.filter((song) => songIdLookup.has(song.id));


    res.set('Content-Type', 'application/json');
    res.send({
      songs: songsToAdd,
    });

    if (jamId) {
      for (const songId of songIds) {
        if (!jamStore.isSongInJam(songId, jamId)) {

          ioServer.emit('SONG_ADDED', {
            songId,
            jamId,
          });

          jamStore.addSongToJam(songId, jamId);
        }
      }
    }
  });

  api.post('/jams/create', (req, res) => {

    const jam = jamStore.createJam();

    res.set('Content-Type', 'application/json');

    res.send({
      jam
    });

    ioServer.emit('JAM_CREATED', jam);
  });

  api.post('/jams/leave', (req, res) => {

    const {
      jamId,
      indId,
    } = req.body;

    jamStore.leaveJam(indId, jamId);
    idStore.leaveJam(indId);

    res.set('Content-Type', 'application/json');
    res.send({
      left: true,
    });

    ioServer.emit('JAM_LEFT', {
      jamId,
      indId,
    });

  });

  api.get('/playlists/:playlistId', (req, res) => {

    const {
      playlistId
    } = req.params;

    const songs = playlistStore.getSongs(playlistId);

    res.set('Content-Type', 'application/json');
    res.send({
      songs
    });

  });

  api.post('/jams/join', (req, res) => {

    const {
      jamId,
      indId,
    } = req.body;

    const songIds = new Set(jamStore.getJam(jamId).songs);

    idStore.joinJam(indId, jamId);
    jamStore.joinJam(indId, jamId);

    res.set('Content-Type', 'application/json');
    res.send({
      songs: songs.filter((song) => songIds.has(song.id))
    });

    ioServer.emit('JAM_JOINED', {
      jamId,
      indId,
    });
  });

  api.post('/song/:songId', (req, res) => {

    const {
      songId,
    } = req.params;

    const {
      jamId,
    } = req.body;

    res.set('Content-Type', 'application/json');
    res.send({
      song: songs.find(song => song.id === songId)
    });

    if (jamId) {
      if (!jamStore.isSongInJam(songId, jamId)) {

        ioServer.emit('SONG_ADDED', {
          songId,
          jamId,
        });

        jamStore.addSongToJam(songId, jamId);
      }
    }
  });

  api.get('/everything', (_, res) => {

    res.set('Content-Type', 'application/json');
    res.send({
      songs
    });
  });

  api.get('/status', (_, res) => {

    res.set('Content-Type', 'application/json');
    res.send({
      jams: jamStore.availableJams(),
      individuals: idStore.allInds(),
    });
  });

  app.use('/api', api);

  //Static file serving
  //-------------------
  app.use('/', express.static(path.join(__dirname, '../webapp')));

  httpServer.listen(PORT, () => {
    log(`HTTP Server listening on port ${PORT}`);
  });
};

start();
