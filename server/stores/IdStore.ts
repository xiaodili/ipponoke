const _ = require('lodash');
const crypto = require('crypto');

interface Ind {
  id: string;
  socketId: string;
  jamId?: string;
}

export class IdStore {

  private individuals: { [indId: string]: Ind } = {};

  public allInds(): Ind[] {

    return _.values(this.individuals);
  }

  public joinJam(indId: string, jamId: string) {

    this.individuals[indId].jamId = jamId;
  };

  public leaveJam(indId: string) {

    delete this.individuals[indId].jamId;
  };

  public add(indId: string, socketId: string) {

    this.individuals[indId] = {
      socketId,
      id: indId,
    };
  }

  public remove(socketId: string): Ind {

    const ind = _.values(this.individuals)
      .find((ind: Ind) => ind.socketId === socketId);

    delete this.individuals[ind.id];

    return ind;
  }

  public generate(): string {

    return crypto.randomBytes(8).toString('hex');
  }
}
