const _ = require('lodash');
const crypto = require('crypto');

const { jamNames } = require('../lib/utils');

interface Jam {
  songs: string[];
  participants: string[];
  id: string;
  name: string;
};

export class JamStore {

  private jams: { [jamId: string]: Jam } = {};

  public availableJams(): Jam[] {
    return _.values(this.jams);
  }

  public addSongToJam(songId: string, jamId: string) {
    this.jams[jamId].songs.push(songId);
  };

  public isSongInJam(songId: string, jamId: string): boolean {
    const songIdx = this.jams[jamId].songs.indexOf(songId);
    return songIdx > -1;
  }

  public removeSongFromJam(songId: string, jamId: string) {
    const jam = this.jams[jamId];
    const songIdx = jam.songs.indexOf(songId);
    jam.songs.splice(songIdx, 1);
  };

  public getAvailableJams(): { [jamId: string]: Jam } {
    return this.jams;
  }

  public getJam(jamId: string): Jam {
    const jam = this.jams[jamId];
    return jam;
  }

  public removeIndividualFromJam(indId: string, jamId: string) {
    const jam = this.jams[jamId];
    const indIdx = jam.participants.indexOf(indId);
    jam.participants.splice(indIdx, 1);
  }

  public createJam(jamName?: string, songs: string[] = []) {

    const jamId = crypto.randomBytes(4).toString('hex');

    const jam: Jam = {
      songs,
      participants: [],
      id: jamId,
      name: jamName || jamNames[Math.floor(Math.random() * jamNames.length)],
    };

    this.jams[jamId] = jam;

    return jam;
  }

  public joinJam(indId: string, jamId: string) {

    this.jams[jamId].participants.push(indId);
  }

  public leaveJam(indId: string, jamId: string) {

    const jam = this.jams[jamId];
    const participantIdx = jam.participants.indexOf(indId);
    this.jams[jamId].participants.splice(participantIdx);
  }
}
