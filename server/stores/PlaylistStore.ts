const { stringToKebabCase } = require('../lib/utils');

export class PlaylistStore {

  private songs: any[];
  private playlists: any[];

  constructor(songs: any[]) {

    this.songs = songs;

    const getSongByTitle = (title: string) => {
      return songs.find((song: any) => {
        return song.title === title;
      });
    };

    this.playlists = [{
      name: 'Keys in the Life of Song',
      description: 'All the minor and major key standards',
      songs: [
        'Piano Man', // C
        'Rolling In The Deep', // Cm
        'Sweet Child O\' Mine', // Db
        'Counting Stars', // C#m
        'Deathly', // D
        '21 Guns', // Dm
        'Fix You', // Eb
        'Civil War', // Ebm
        'Under the Bridge', // E
        'Scarborough Fair', // Em
        'Stupid Thing', // F
        'My Skin', // Fm
        'You Belong With Me', // Gb
        'Numb', // F#m
        'Let Her Go', // G
        'Love The Way You Lie', // Gm
        'Viva La Vida', // Ab
        'Heart-Shaped Box', // G#m
        'Someone Like You', // A
        'House of the Rising Sun', // Am
        'Circle of Life', // Bb
        'Here Without You', // Bbm
        'Whatcha Say', // B
        'Comfortably Numb', // Bm
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Any Major Key Will Tell You',
      description: 'All the major standards',
      songs: [
        'Piano Man', // C
        'Sweet Child O\' Mine', // Db
        'Deathly', // D
        'Fix You', // Eb
        'Under the Bridge', // E
        'Stupid Thing', // F
        'You Belong With Me', // Gb
        'Let Her Go', // G
        'Viva La Vida', // Ab
        'Someone Like You', // A
        'Circle of Life', // Bb
        'Whatcha Say', // B
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'It\'s Just A Minor Thing',
      description: 'All the minor standards',
      songs: [
        'Rolling In The Deep', // Cm
        'Counting Stars', // C#m
        '21 Guns', // Dm
        'Civil War', // Ebm
        'Scarborough Fair', // Em
        'My Skin', // Fm
        'Numb', // F#m
        'Love The Way You Lie', // Gm
        'Heart-Shaped Box', // G#m
        'House of the Rising Sun', // Am
        'Here Without You', // Bbm
        'Comfortably Numb', // Bm
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Soul',
      description: 'Soul',
      songs: [
        'Dark End Of The Street', // G
        'Stand By Me', // A
        'Drift Away', // B
        'Midnight Train to Georgia', // Db
        'Ooh Child', // Bb
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Jazz',
      description: 'Not stupid',
      songs: [
        'Fly Me To The Moon', // Am
        'Come Away with Me', // C
        'Moon River', // F
        'What A Wonderful World', // F
        'Round Midnight', // Bbm
        'Summertime', // Bm
        'Somewhere Over the Rainbow', // G
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Instrumental',
      description: 'Instrumental',
      songs: [
        'Comptine d\'un autre ete: l\'apres midi', // Em
        'He\'s a Pirate', // Dm
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Ballads',
      description: 'Ballads',
      songs: [
        'Livin\' on a Prayer', // Em
        'I\'d Do Anything For Love', // D
        '21 Guns', // Dm
        'Someone Like You', // A
        'November Rain', // E
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Cindies',
      description: 'Beats',
      songs: [
        'Firework', // Ab
        "Don't Stop Believin'", // E
        "Livin' on a Prayer", // Em
        'Symphony', // Eb
        'Numb', // F#m
      ].map(getSongByTitle).filter(s => s)
    }, {
      name: 'Bears, Beets, Battlestar Galactica',
      description: 'The Office Soundtrack',
      songs: [
        'Tiny Dancer', //C
        'Drift Away', // B
        'Goodbye My Lover', // E
        'Boulevard of Broken Dreams', // Fm
        'Take Me Home, Country Roads', // G
        'Kind & Generous', // Eb
        'American Pie', // G
        'Closing Time', // G
        'Karma Chameleon', // Bb
        'Baby I Love Your Way', // G
        'I Will Remember You', // Eb
      ].map(getSongByTitle).filter(s => s)
    }].map(p => ({ // Finally, id-ifying this
      ...p,
      id: stringToKebabCase(p.name)
    }));
  }

  public getPlaylists() {

    return this.playlists.map((p, i) => ({
      name: p.name,
      id: stringToKebabCase(p.name),
      songs: p.songs.map((s: any) => ({
        id: s.id,
        title: s.title,
        artist: s.metadata.artist,
      }))
    }));
  }

  public getSongs(playlistId: string) {

    return this.playlists.find((playlist) => {
      return playlist.id === playlistId;
    }).songs;
  }
}
