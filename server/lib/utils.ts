export const log = (...obj: any) => {
  console.log(`[${new Date().toISOString()}]`, ...obj);
}

export const containsYoutubeTimings = (song: any) => {
  for (const section of song.sections) {
    if (section.metadata["youtube-time"]) {
      return true;
    }
  }
  return false;
}

export const stringToKebabCase = (str: string) => {

  // e.g. I don't think → i-dont-think
  return str
    .toLowerCase()
    .replace(/ /g, '-')
    .replace(/['":]/g, '');
};

export class IdGenerator {

  constructor() {
    this.idify = this.idify.bind(this);
  }

  private ids = new Set();

  public idify(song: any): any {

    const generatedId = stringToKebabCase(song.metadata.artist + ' ' + song.title);

    if (this.ids.has(generatedId)) {
      console.log(`Warning: duplicate ID encountered: ${generatedId}`);
    } else {
      this.ids.add(generatedId);
    }

    return {
      id: generatedId,
      ...song
    };
  }
}

export const jamNames = [
  'Air-Guitaring with Angus',
  'Bopping with Björk',
  'Busking with Blur',
  'Crooning with Coldplay',
  'Duckwalking with Dido',
  'Enunciating with Eminem',
  'Jamming with Jimi',
  'Grooving with Gorillaz',
  'Headbanging with Herbie',
  'Harmonising with Hanson',
  'Moshing with Megadeth',
  'Noodling with Nickelback',
  'Posing with Prince',
  'Rocking with Rush',
  'Shoegazing with Stevie',
  'Shredding with Slash',
  'Twerking with Townshend',
];
