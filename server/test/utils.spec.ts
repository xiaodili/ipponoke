import { expect } from 'chai';
import {
  describe,
  it,
} from 'mocha';

import {
  containsYoutubeTimings,
} from '../lib/utils';

describe('containsYoutubeTimings', () => {
  it('should infer whether a song has youtube-time metadata', () => {

    const songDataNoYoutubeTimings = {
      "id": "moon-river",
      "title": "Moon River",
      "metadata": {
        "artist": "Audrey Hepburn",
        "key": "F",
        "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
      },
      "sections": [{
        "name": "Verse 1",
        "metadata": {},
        "lines": [
          "F    Dm     A#           F          ",
          "Moon River, wider than a mile,  I'm",
          "A#              F            Gm/E  A7     ",
          "crossing you in style some   day.      Old",
          "Dm    F7         A#   D#9-5          ",
          "dream-maker, you heartbreaker, Where-",
          "Dm   Dm7    Dm7/B E7      ",
          "ever you're goin’,     I’m",
          "Am7   D7      Gm7   C9  ",
          "goin’    your way."
        ]
      }]
    };
    expect(containsYoutubeTimings(songDataNoYoutubeTimings)).to.equal(false);

    const songDataWithYoutubeTimings = {
      "id": "moon-river",
      "title": "Moon River",
      "metadata": {
        "artist": "Audrey Hepburn",
        "key": "F",
        "link": "https://www.youtube.com/watch?v=0BfUDyvdTSE"
      },
      "sections": [{
        "name": "Verse 1",
        "metadata": {
          "youtube-time": "0:15"
        },
        "lines": [
          "F    Dm     A#           F          ",
          "Moon River, wider than a mile,  I'm",
          "A#              F            Gm/E  A7     ",
          "crossing you in style some   day.      Old",
          "Dm    F7         A#   D#9-5          ",
          "dream-maker, you heartbreaker, Where-",
          "Dm   Dm7    Dm7/B E7      ",
          "ever you're goin’,     I’m",
          "Am7   D7      Gm7   C9  ",
          "goin’    your way."
        ]
      }]
    };
    expect(containsYoutubeTimings(songDataWithYoutubeTimings)).to.equal(true);
  })
});
